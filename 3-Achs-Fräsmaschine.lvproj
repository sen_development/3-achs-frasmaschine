﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LV.ExampleFinder" Type="Str">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;ExampleProgram&gt;
&lt;Title&gt;
	&lt;Text Locale="US"&gt;9503 Stepper Drive (Velocity Setpoint).lvproj&lt;/Text&gt;
&lt;/Title&gt;
&lt;Keywords&gt;
	&lt;Item&gt;9503&lt;/Item&gt;
	&lt;Item&gt;stepper&lt;/Item&gt;
	&lt;Item&gt;velocity&lt;/Item&gt;
	&lt;Item&gt;drive&lt;/Item&gt;
	&lt;Item&gt;motion&lt;/Item&gt;
	&lt;Item&gt;SoftMotion&lt;/Item&gt;
&lt;/Keywords&gt;
&lt;Navigation&gt;
	&lt;Item&gt;3410&lt;/Item&gt;
	&lt;Item&gt;3411&lt;/Item&gt;
	&lt;Item&gt;3412&lt;/Item&gt;
&lt;/Navigation&gt;
&lt;FileType&gt;LV Project&lt;/FileType&gt;
&lt;Metadata&gt;
&lt;Item Name="RTSupport"&gt;LV Project&lt;/Item&gt;
&lt;/Metadata&gt;
&lt;ProgrammingLanguages&gt;
&lt;Item&gt;LabVIEW&lt;/Item&gt;
&lt;/ProgrammingLanguages&gt;
&lt;RequiredSoftware&gt;
&lt;NiSoftware MinVersion="11.0"&gt;LabVIEW&lt;/NiSoftware&gt; 
&lt;/RequiredSoftware&gt;
&lt;RequiredMotionHardware&gt;
&lt;Device&gt;
&lt;Model&gt;751F&lt;/Model&gt;
&lt;/Device&gt;
&lt;/RequiredMotionHardware&gt;
&lt;/ExampleProgram&gt;</Property>
	<Property Name="NI.Project.Description" Type="Str">This example demonstrates how to use an NI 9503 C Series drive module in velocity mode. This is the simplest NI 9503 example and is suitable for applications that do slow velocity moves.

For applications requiring complex moves, coordinated moves, or SoftMotion axis integration, consider using one of the 9503 Stepper Drive (Position Profile).lvproj examples.

This example needs to be compiled for a specific FPGA target before use.

For information on moving this example to another FPGA target, refer to ni.com/info and enter info code fpgaex.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{018050AF-4A5D-4695-93A0-B29BD6B5884B}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.Velocity Feedback</Property>
	<Property Name="varPersistentID:{019DB736-A7DE-494B-B43F-3F0D4B2AF83E}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.DIO Control (Port A)</Property>
	<Property Name="varPersistentID:{03BEB568-00AB-4F84-91D9-7DEBD7745785}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.DIO Status (Port A)</Property>
	<Property Name="varPersistentID:{0501627F-00B6-47F0-90BC-2BE8B5E7A311}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.DIO Control (Port A)</Property>
	<Property Name="varPersistentID:{0692285F-127A-4E7F-9AF4-5071AAC26653}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.Mailbox (FPGA to Host)</Property>
	<Property Name="varPersistentID:{076B7B97-D7D1-4EAF-BFEC-BC749C52F5E8}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.Steps Generated</Property>
	<Property Name="varPersistentID:{084FAE43-31DB-4B1A-8F16-10E3C883697B}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.Mailbox (Host to FPGA)</Property>
	<Property Name="varPersistentID:{0BD2D2AE-5E2A-4245-AAD0-E5FB6A013BC5}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.DIO Control (Port B)</Property>
	<Property Name="varPersistentID:{0EBA29D7-4645-4A7B-8C1C-3AD95C370398}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.Control Register</Property>
	<Property Name="varPersistentID:{1E3E8E9F-7402-4B4E-8ED0-6E860748A445}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.Position Setpoint</Property>
	<Property Name="varPersistentID:{405BB286-C754-4B84-87AD-93843D17E8DD}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.DIO Status (Port B)</Property>
	<Property Name="varPersistentID:{42EDA51E-AEB5-4A13-B710-EBF7530012D7}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.Status Register</Property>
	<Property Name="varPersistentID:{47C8E6EC-9AE8-47BE-95E5-64C5EEBA15EE}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.DIO Control (Port A)</Property>
	<Property Name="varPersistentID:{4CDB463B-DE07-4C51-A254-A2ABC63E1CEA}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.Velocity Feedback</Property>
	<Property Name="varPersistentID:{4F118B9D-EA6E-4D85-AEB4-B01FC2CF0676}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.Mailbox (Host to FPGA)</Property>
	<Property Name="varPersistentID:{526FFDC0-9387-47BD-802D-A80E44D8DA84}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.Mailbox (FPGA to Host)</Property>
	<Property Name="varPersistentID:{6E9B7C36-1EEF-4407-AACB-15EC12774AC7}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.DIO Status (Port A)</Property>
	<Property Name="varPersistentID:{6FA5853D-7C3D-4A36-9EE6-045918AD9BA3}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.Steps Generated</Property>
	<Property Name="varPersistentID:{7F60D8E9-8712-4826-BCE8-12BDB3693997}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.Mailbox (FPGA to Host)</Property>
	<Property Name="varPersistentID:{8BBBF1CE-94D4-4F60-93FD-A0BC88720462}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.Mailbox (Host to FPGA)</Property>
	<Property Name="varPersistentID:{962CF898-8A40-412F-AA84-112002751495}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.Control Register</Property>
	<Property Name="varPersistentID:{9D6BD750-989F-4DEB-B1C9-9AC2DAF89241}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.DIO Status (Port B)</Property>
	<Property Name="varPersistentID:{A7667ECF-4622-4628-BA2E-19B29523EF12}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.Position Feedback</Property>
	<Property Name="varPersistentID:{AF7C6CDE-C8D5-4CDA-A619-E98D8588E772}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.Status Register</Property>
	<Property Name="varPersistentID:{AFE6BFBD-A2CA-45D7-B2B4-736C505AFE7F}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.Position Feedback</Property>
	<Property Name="varPersistentID:{B74849F5-BC0D-4BE1-92F4-D703472661C8}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.Position Setpoint</Property>
	<Property Name="varPersistentID:{B852E9DE-D63B-4F72-A4E2-910638F90D97}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.DIO Status (Port A)</Property>
	<Property Name="varPersistentID:{CD1E0297-DF3F-475F-9BED-A2D71A1FD00B}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.Steps Generated</Property>
	<Property Name="varPersistentID:{CE8D7024-9FA8-4949-BAD8-C11BEEDE95B2}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 2.Control Register</Property>
	<Property Name="varPersistentID:{CEB49FD6-4A20-439D-BDC3-0B7B3F7B0995}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.DIO Control (Port B)</Property>
	<Property Name="varPersistentID:{CEC9430A-E5A1-4AA6-85D4-F79CDCF4D09B}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.DIO Status (Port B)</Property>
	<Property Name="varPersistentID:{D2CCEF16-CFBD-4FC5-95F5-3BC50935A3CA}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.Velocity Feedback</Property>
	<Property Name="varPersistentID:{DAEE1876-B1E2-40DE-BCE4-C3313860E7EA}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.Position Setpoint</Property>
	<Property Name="varPersistentID:{F38E39F9-1A06-429F-87B5-60DA2B7B682A}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 3.Position Feedback</Property>
	<Property Name="varPersistentID:{F7E7D478-2C68-4740-A80A-02A3859EDDE4}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.Status Register</Property>
	<Property Name="varPersistentID:{FB68832D-FD75-4AA1-8139-C292A3BE9FB7}" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/Real-Time Scan Resources/User-Defined Variables/Axis 1.DIO Control (Port B)</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">true</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Support VIs" Type="Folder">
			<Item Name="Accel Conversion RevPerSec_2 to StepsPerPeriod_2.vi" Type="VI" URL="../support/Accel Conversion RevPerSec_2 to StepsPerPeriod_2.vi"/>
			<Item Name="Calculate Current Loop Gains Set 1.vi" Type="VI" URL="../support/Calculate Current Loop Gains Set 1.vi"/>
			<Item Name="Calculate Current Scheduler Coefficients.vi" Type="VI" URL="../support/Calculate Current Scheduler Coefficients.vi"/>
			<Item Name="Velocity Conversion RPS to StepsPerPeriod.vi" Type="VI" URL="../support/Velocity Conversion RPS to StepsPerPeriod.vi"/>
			<Item Name="Velocity Conversion StepsPerPeriod to RPS.vi" Type="VI" URL="../support/Velocity Conversion StepsPerPeriod to RPS.vi"/>
			<Item Name="Zero with Tolerance.vi" Type="VI" URL="../support/Zero with Tolerance.vi"/>
		</Item>
		<Item Name="Host.vi" Type="VI" URL="../Host/Host.vi"/>
		<Item Name="HostProcessData.ctl" Type="VI" URL="../TypeDefs/HostProcessData.ctl"/>
		<Item Name="HostState.ctl" Type="VI" URL="../TypeDefs/HostState.ctl"/>
		<Item Name="NI 9503 Stepper Drive Velocity Setpoint.html" Type="Document" URL="../documentation/NI 9503 Stepper Drive Velocity Setpoint.html"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_SoftMotion_MotorControlIP.lvlib" Type="Library" URL="/&lt;vilib&gt;/Motion/MotorControl/NI_SoftMotion_MotorControlIP.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="NI-cRIO-9056-01DEC88F" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">NI-cRIO-9056-01DEC88F</Property>
		<Property Name="alias.value" Type="Str">192.168.4.158</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,Linux;CPU,x64;DeviceCode,79DF;</Property>
		<Property Name="crio.ControllerPID" Type="Str">79DF</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">9</Property>
		<Property Name="host.TargetOSID" Type="UInt">19</Property>
		<Property Name="host.TargetUIEnabled" Type="Bool">false</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/home/lvuser/natinst/bin/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">true</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/home/lvuser/natinst/bin</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">Listen 8000

NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
WorkerLimit 10
InactivityTimeout 60

LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule

#
# Pipeline Definition
#

SetConnector netConnector

AddHandler LVAuth
AddHandler LVRFP

AddHandler fileHandler ""

AddOutputFilter chunkFilter


</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">fpga</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9056</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Real-Time Scan Resources" Type="Module Container">
				<Property Name="crio.ModuleContainerType" Type="Str">crio.RSIModuleContainer</Property>
				<Item Name="User-Defined Variables" Type="cRIO IO Variable Container">
					<Item Name="Axis 1.Control Register" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to control the state of the device and the operating modes.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{936546CE-563B-48D1-A9F9-78D46743E3C7}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.Control Register/datatype=6/direction=1/index=0</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.Mailbox (FPGA to Host)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to provide configuration and other data from FPGA to SoftMotion.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A6F7D6E6-D7C0-4797-90A0-440D9499B717}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.Mailbox (FPGA to Host)/datatype=5/direction=0/index=0</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"A!A!!!!!!"!!5!#!!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.Mailbox (Host to FPGA)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to provide configuration and other data from SoftMotion to FPGA.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{407E808C-C7FA-4964-9B71-E150D4214690}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.Mailbox (Host to FPGA)/datatype=5/direction=1/index=1</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"A!A!!!!!!"!!5!#!!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.Position Setpoint" Type="Variable">
						<Property Name="Description:Description" Type="Str">Specifies the new setpoint position.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CBE28825-E761-45A3-9161-325B01DEDB16}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=2</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">6!!!!"A!A!!!!!!"!$Q!8Q.2!%!!!!!A!!%!1!!!!##!!!!!!!!!!!!"!%!!!!!A@`````````]!!!!"````Y1!!!!!!!!!"!!%!!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.Status Register" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to report status and operation mode information from the device to the system.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B3F70B56-55F8-4007-B34E-DE323353B34B}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.Status Register/datatype=6/direction=0/index=1</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.Position Feedback" Type="Variable">
						<Property Name="Description:Description" Type="Str">Returns the position of the axis or coordinate in Units.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DC27D298-593E-4DE3-ADE3-28D6C6870856}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.Position Feedback/datatype=2/direction=0/index=2</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.Steps Generated" Type="Variable">
						<Property Name="Description:Description" Type="Str">Returns the total number of steps generated for the axis.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{854317AC-436F-4D15-B272-1B28B36E5683}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.Steps Generated/datatype=2/direction=0/index=3</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.DIO Control (Port A)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains the values of digital outputs 0 through 3 as well as information to clear input latches for limits, home, and digital inputs 0 through 3.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{63CAEC4C-4375-4E8B-8D45-9BD560A34959}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.DIO Control (Port A)/datatype=6/direction=1/index=3</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.DIO Status (Port A)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to report value and latching information for limits, home, and digital inputs 0 through 3.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{59F18F21-D34A-440E-BF0C-DADCE1F6BEE6}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.DIO Status (Port A)/datatype=6/direction=0/index=4</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.DIO Control (Port B)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains the values of digital outputs 4 through 7 as well as information to enable position compare and position capture operations and clear input latches for position capture and digital inputs 4 through 7.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{03630C58-CD97-4F3E-8BE6-E29784853061}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.DIO Control (Port B)/datatype=6/direction=1/index=4</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.DIO Status (Port B)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to report value and latching information for position capture, position compare, and digital inputs 4 through 7.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B3D43C34-FEFB-4D29-A9DC-E6790F105CD2}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.DIO Status (Port B)/datatype=6/direction=0/index=5</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 1.Velocity Feedback" Type="Variable">
						<Property Name="Description:Description" Type="Str">Specifies the velocity derived from the position feedback. The units are counts/PID loop rate time.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B3C7F845-41F2-446C-8C5C-FE57C9CF43BD}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 1.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=6</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">6!!!!"A!A!!!!!!"!$Q!8Q.2!%!!!!!A!!%!1!!!!##!!!!!!!!!!!!"!%!!!!!A@`````````]!!!!"````Y1!!!!!!!!!"!!%!!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.Control Register" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to control the state of the device and the operating modes.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4813C9AE-1626-40E2-A2D1-92C893C9653A}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.Control Register/datatype=6/direction=1/index=5</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.Mailbox (FPGA to Host)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to provide configuration and other data from FPGA to SoftMotion.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4A50DE0C-31A7-40D3-AFC5-1226FDA68401}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.Mailbox (FPGA to Host)/datatype=5/direction=0/index=7</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"A!A!!!!!!"!!5!#!!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.Mailbox (Host to FPGA)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to provide configuration and other data from SoftMotion to FPGA.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{33EE390F-78F9-400C-B704-58C0D7A04188}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.Mailbox (Host to FPGA)/datatype=5/direction=1/index=6</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"A!A!!!!!!"!!5!#!!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.Position Setpoint" Type="Variable">
						<Property Name="Description:Description" Type="Str">Specifies the new setpoint position.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0337EAE1-9570-4F55-9DC5-6B925C4780EE}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=7</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">6!!!!"A!A!!!!!!"!$Q!8Q.2!%!!!!!A!!%!1!!!!##!!!!!!!!!!!!"!%!!!!!A@`````````]!!!!"````Y1!!!!!!!!!"!!%!!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.Status Register" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to report status and operation mode information from the device to the system.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E44C817D-F6DC-401A-85D4-F3B9ACD9A4D2}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.Status Register/datatype=6/direction=0/index=8</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.Position Feedback" Type="Variable">
						<Property Name="Description:Description" Type="Str">Returns the position of the axis or coordinate in Units.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2DE03463-12E0-48A1-B408-73CC91ED7D0A}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.Position Feedback/datatype=2/direction=0/index=9</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.Steps Generated" Type="Variable">
						<Property Name="Description:Description" Type="Str">Returns the total number of steps generated for the axis.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B6256622-F944-4E75-BCCE-DAF23D644E65}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.Steps Generated/datatype=2/direction=0/index=10</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.DIO Control (Port A)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains the values of digital outputs 0 through 3 as well as information to clear input latches for limits, home, and digital inputs 0 through 3.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0DF8051B-C1F4-4EB5-898D-AF590BD87F25}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.DIO Control (Port A)/datatype=6/direction=1/index=8</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.DIO Status (Port A)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to report value and latching information for limits, home, and digital inputs 0 through 3.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CA2DB12E-EF97-4735-811B-F3D08E3AC19E}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.DIO Status (Port A)/datatype=6/direction=0/index=11</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.DIO Control (Port B)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains the values of digital outputs 4 through 7 as well as information to enable position compare and position capture operations and clear input latches for position capture and digital inputs 4 through 7.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9AD3274D-E096-44FC-808D-806587084876}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.DIO Control (Port B)/datatype=6/direction=1/index=9</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.DIO Status (Port B)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to report value and latching information for position capture, position compare, and digital inputs 4 through 7.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9F67DCC6-8812-4A91-93DC-A6B351C8DC37}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.DIO Status (Port B)/datatype=6/direction=0/index=12</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 2.Velocity Feedback" Type="Variable">
						<Property Name="Description:Description" Type="Str">Specifies the velocity derived from the position feedback. The units are counts/PID loop rate time.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DA0FAA92-F7C0-45E7-958B-A2F6111D9869}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 2.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=13</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">6!!!!"A!A!!!!!!"!$Q!8Q.2!%!!!!!A!!%!1!!!!##!!!!!!!!!!!!"!%!!!!!A@`````````]!!!!"````Y1!!!!!!!!!"!!%!!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.Control Register" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to control the state of the device and the operating modes.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D14BE3AE-4EFB-46E7-B0D7-0733C33D8736}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.Control Register/datatype=6/direction=1/index=10</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.Mailbox (FPGA to Host)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to provide configuration and other data from FPGA to SoftMotion.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{59F7A552-8DE9-40A7-BF40-124E0354177F}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.Mailbox (FPGA to Host)/datatype=5/direction=0/index=14</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"A!A!!!!!!"!!5!#!!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.Mailbox (Host to FPGA)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to provide configuration and other data from SoftMotion to FPGA.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C5FAB816-19EE-4821-B57F-41707B179D4F}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.Mailbox (Host to FPGA)/datatype=5/direction=1/index=11</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"A!A!!!!!!"!!5!#!!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.Position Setpoint" Type="Variable">
						<Property Name="Description:Description" Type="Str">Specifies the new setpoint position.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F6425AA6-51D3-4206-B29C-5B86EA0DE4A8}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=12</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">6!!!!"A!A!!!!!!"!$Q!8Q.2!%!!!!!A!!%!1!!!!##!!!!!!!!!!!!"!%!!!!!A@`````````]!!!!"````Y1!!!!!!!!!"!!%!!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.Status Register" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to report status and operation mode information from the device to the system.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DC670331-7130-4FF8-ACD8-BB5DF85B84A1}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.Status Register/datatype=6/direction=0/index=15</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.Position Feedback" Type="Variable">
						<Property Name="Description:Description" Type="Str">Returns the position of the axis or coordinate in Units.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A0E4D4FD-3884-43CF-86A1-CCA8C2079A1B}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">16</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.Position Feedback/datatype=2/direction=0/index=16</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.Steps Generated" Type="Variable">
						<Property Name="Description:Description" Type="Str">Returns the total number of steps generated for the axis.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5E79850A-008E-435E-8EC3-FC448F4B7752}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">17</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.Steps Generated/datatype=2/direction=0/index=17</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.DIO Control (Port A)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains the values of digital outputs 0 through 3 as well as information to clear input latches for limits, home, and digital inputs 0 through 3.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{30AA07C1-223C-4AB8-A1A9-DB4EAE61F437}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.DIO Control (Port A)/datatype=6/direction=1/index=13</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.DIO Status (Port A)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to report value and latching information for limits, home, and digital inputs 0 through 3.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E6B28907-AA6D-4D8C-BED7-04C972B9C9FF}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">18</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.DIO Status (Port A)/datatype=6/direction=0/index=18</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.DIO Control (Port B)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains the values of digital outputs 4 through 7 as well as information to enable position compare and position capture operations and clear input latches for position capture and digital inputs 4 through 7.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{79BC5C5E-9EFF-4453-8DEC-800FCBFA2543}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.DIO Control (Port B)/datatype=6/direction=1/index=14</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.DIO Status (Port B)" Type="Variable">
						<Property Name="Description:Description" Type="Str">Contains information used to report value and latching information for position capture, position compare, and digital inputs 4 through 7.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4281C939-6849-4542-8375-DC854108F893}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">19</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.DIO Status (Port B)/datatype=6/direction=0/index=19</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">'1!!!"A!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="Axis 3.Velocity Feedback" Type="Variable">
						<Property Name="Description:Description" Type="Str">Specifies the velocity derived from the position feedback. The units are counts/PID loop rate time.</Property>
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C3745DB5-E97B-4E0B-BDD0-6E24EBE686D2}</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">20</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:RSI" Type="Str">True</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">/name=Axis 3.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=20</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">6!!!!"A!A!!!!!!"!$Q!8Q.2!%!!!!!A!!%!1!!!!##!!!!!!!!!!!!"!%!!!!!A@`````````]!!!!"````Y1!!!!!!!!!"!!%!!!!!!!!!!!!!!!!!!!</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Real-Time Resources" Type="Module Container">
				<Property Name="crio.ModuleContainerType" Type="Str">crio.DAQModuleContainer</Property>
			</Item>
			<Item Name="FPGA Target" Type="FPGA Target">
				<Property Name="AutoRun" Type="Bool">false</Property>
				<Property Name="configString.guid" Type="Str">{0017E7D9-AF68-4A4D-BDCB-E0F80A595821}resource=/crio_Mod1/DI14;0;ReadMethodType=bool{01ADC950-F697-4E10-AC0F-3597AE707FCD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9375,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0337EAE1-9570-4F55-9DC5-6B925C4780EE}/name=Axis 2.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=7{03630C58-CD97-4F3E-8BE6-E29784853061}/name=Axis 1.DIO Control (Port B)/datatype=6/direction=1/index=4{047258CE-911D-42C2-A57D-DE27BB9E6FEC}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=bool{05A81A96-A932-488D-9473-888AF8C3AB12}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{09AB7A74-FAD4-439D-B637-9EF3ADAA2D7E}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64{0CF72225-8BCB-413F-AF38-51EA4654D1B6}resource=/crio_Mod3/User LED;0;WriteMethodType=bool{0D6D93D5-D434-4FF8-8977-5D8277810B89}resource=/crio_Mod2/Phase A Neg;0;WriteMethodType=bool{0DF8051B-C1F4-4EB5-898D-AF590BD87F25}/name=Axis 2.DIO Control (Port A)/datatype=6/direction=1/index=8{12F80F9D-1A19-493C-A636-E7CF8033A12E}resource=/crio_Mod1/DI15:0;0;ReadMethodType=u16{131E4BA5-61B4-4357-971A-4160B95447E5}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{13433233-B1E3-41EE-843C-6F079D8DA1CE}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=bool{15176B56-6AE9-44B1-B312-A218C64C0A3E}resource=/crio_Mod1/DI13;0;ReadMethodType=bool{1731DCD7-A5EC-4931-9123-1CE53C50FCB3}resource=/crio_Mod1/DI7;0;ReadMethodType=bool{18DCE8CD-C41D-4393-9EF9-0068DAF3E454}resource=/crio_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=bool{196A8036-32AD-4C7A-B6FD-2089C426A26F}resource=/crio_Mod1/DO9;0;ReadMethodType=bool;WriteMethodType=bool{1B64D359-A52B-4F6E-84B9-E2C60FBBF03B}resource=/Chassis Temperature;0;ReadMethodType=i16{1CBD5F71-337D-444F-A614-F4E3D2FC5266}resource=/crio_Mod3/Phase B Pos;0;WriteMethodType=bool{1E4D6A2C-5B1C-4B24-8F0F-D0DE4861E24C}resource=/crio_Mod1/DO15:0;0;ReadMethodType=u16;WriteMethodType=u16{1F296AF1-93A2-4B75-9EE0-63DE3E43B3CD}resource=/crio_Mod3/Phase B Current;0;ReadMethodType=I16{22F14F7B-7878-4735-8FFF-6E208B901DCB}resource=/crio_Mod2/User LED;0;WriteMethodType=bool{29E50DF6-D6C6-4E70-8683-467F7D2BA535}resource=/crio_Mod2/Phase B Pos;0;WriteMethodType=bool{2A7D546E-C337-47B4-86F2-EC01EB73A8D4}resource=/crio_Mod4/Phase A Neg;0;WriteMethodType=bool{2DC7761A-7739-4BFC-982B-7E45F47099A2}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32{2DE03463-12E0-48A1-B408-73CC91ED7D0A}/name=Axis 2.Position Feedback/datatype=2/direction=0/index=9{30AA07C1-223C-4AB8-A1A9-DB4EAE61F437}/name=Axis 3.DIO Control (Port A)/datatype=6/direction=1/index=13{311CCA04-9C8B-47AE-9291-2427298E19EF}resource=/crio_Mod2/Phase B Current;0;ReadMethodType=I16{33EE390F-78F9-400C-B704-58C0D7A04188}/name=Axis 2.Mailbox (Host to FPGA)/datatype=5/direction=1/index=6{34F7A81C-BA4C-400F-9078-70CD2104C790}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{36639CFF-6AB6-40BD-B425-DA84855425D7}NumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool{36A125B8-5220-4C6B-B2FC-0B3C2F2703F1}resource=/crio_Mod4/Vsup Voltage;0;ReadMethodType=u16{37B9D1F4-8948-4C7D-859E-840CB3C009E8}resource=/crio_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=bool{407E808C-C7FA-4964-9B71-E150D4214690}/name=Axis 1.Mailbox (Host to FPGA)/datatype=5/direction=1/index=1{4281C939-6849-4542-8375-DC854108F893}/name=Axis 3.DIO Status (Port B)/datatype=6/direction=0/index=19{4813C9AE-1626-40E2-A2D1-92C893C9653A}/name=Axis 2.Control Register/datatype=6/direction=1/index=5{4A0A5AD1-CC49-4854-9036-9503E724C780}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=bool{4A50DE0C-31A7-40D3-AFC5-1226FDA68401}/name=Axis 2.Mailbox (FPGA to Host)/datatype=5/direction=0/index=7{514991AE-3F66-447E-8071-01006692529D}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=bool{59CC0C88-22A1-4840-A476-5EC74AD59EEE}resource=/crio_Mod1/DI2;0;ReadMethodType=bool{59F18F21-D34A-440E-BF0C-DADCE1F6BEE6}/name=Axis 1.DIO Status (Port A)/datatype=6/direction=0/index=4{59F7A552-8DE9-40A7-BF40-124E0354177F}/name=Axis 3.Mailbox (FPGA to Host)/datatype=5/direction=0/index=14{5B08D5E4-F4FB-4A3B-85A6-B54AE2A3CB8F}resource=/crio_Mod4/Phase A Pos;0;WriteMethodType=bool{5C0C0298-27DB-474F-89DA-9CCEBB00E7B5}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=bool{5E79850A-008E-435E-8EC3-FC448F4B7752}/name=Axis 3.Steps Generated/datatype=2/direction=0/index=17{63CAEC4C-4375-4E8B-8D45-9BD560A34959}/name=Axis 1.DIO Control (Port A)/datatype=6/direction=1/index=3{66B47596-5DE0-43D1-B697-174EA01F359B}resource=/crio_Mod1/DI10;0;ReadMethodType=bool{697EF40C-5290-4619-AEE3-4E63DDA449D0}resource=/crio_Mod1/DI7:0;0;ReadMethodType=u8{6A747376-939C-40E7-BDB1-CBEA97D3007B}resource=/crio_Mod1/DO11;0;ReadMethodType=bool;WriteMethodType=bool{6CD02D79-DFB0-4E13-B8B0-4BF96F177BEF}resource=/crio_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=bool{6D91CD44-DB04-47C3-A62C-16857B5AA964}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=bool{6E3BA9CA-2C9B-4CB9-B653-CD2595A217C6}resource=/crio_Mod1/DO8;0;ReadMethodType=bool;WriteMethodType=bool{6F7A0866-03D4-4F01-BD82-CFD8BEFAFF79}resource=/crio_Mod4/Phase B Current;0;ReadMethodType=I16{6FBB5A63-EA42-449D-BBFE-CC2C51A9EAD7}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9503[crioConfig.End]{79BC5C5E-9EFF-4453-8DEC-800FCBFA2543}/name=Axis 3.DIO Control (Port B)/datatype=6/direction=1/index=14{7BBA44A1-0B1C-4D3B-8C34-77E5F8BFF19D}resource=/crio_Mod4/Phase B Neg;0;WriteMethodType=bool{7D03D325-9E75-4C28-B764-31C7691A4DDF}resource=/crio_Mod1/DO12;0;ReadMethodType=bool;WriteMethodType=bool{7F9F9F36-C1DE-4CF2-AABE-88FF5FFD8204}resource=/crio_Mod1/DO10;0;ReadMethodType=bool;WriteMethodType=bool{812C894E-0D98-4649-819A-AEA3ED0D8FB3}resource=/crio_Mod1/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{822F4B12-3504-4AB6-91CA-915DF3D744C7}resource=/crio_Mod2/Phase B Neg;0;WriteMethodType=bool{854317AC-436F-4D15-B272-1B28B36E5683}/name=Axis 1.Steps Generated/datatype=2/direction=0/index=3{8845D70A-3D8B-49E0-AEA5-EE3B8141E438}NumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool{88657F19-2BA2-492A-9398-45300D6B8A81}resource=/crio_Mod1/DI11;0;ReadMethodType=bool{89ADC391-1BC0-4EB7-9C7B-BEF6E0B4E2DE}resource=/crio_Mod1/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{92F4FB54-4EEB-4036-A6FB-964EFB5CE42D}resource=/crio_Mod1/DI15;0;ReadMethodType=bool{936546CE-563B-48D1-A9F9-78D46743E3C7}/name=Axis 1.Control Register/datatype=6/direction=1/index=0{9AD3274D-E096-44FC-808D-806587084876}/name=Axis 2.DIO Control (Port B)/datatype=6/direction=1/index=9{9C2165FA-8616-48F6-B956-1A396BED9378}resource=/crio_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=bool{9C71592D-D581-44ED-9946-F12827544A5C}resource=/crio_Mod3/Phase A Pos;0;WriteMethodType=bool{9DA89C10-2764-4628-9BFD-833BB10392D5}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9503[crioConfig.End]{9E0BFAF6-29A4-4BDA-8BB0-50EC671080B5}resource=/crio_Mod2/Phase A Pos;0;WriteMethodType=bool{9F5B802E-B199-494A-AFCA-E189C0C34B3B}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctl{9F67DCC6-8812-4A91-93DC-A6B351C8DC37}/name=Axis 2.DIO Status (Port B)/datatype=6/direction=0/index=12{A0AFFE3F-BCDE-458A-A2FB-6F87F5424577}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=bool{A0DCCCBC-384C-4A97-8439-C5B775CF3C25}resource=/crio_Mod3/Phase A Neg;0;WriteMethodType=bool{A0E4D4FD-3884-43CF-86A1-CCA8C2079A1B}/name=Axis 3.Position Feedback/datatype=2/direction=0/index=16{A6F7D6E6-D7C0-4797-90A0-440D9499B717}/name=Axis 1.Mailbox (FPGA to Host)/datatype=5/direction=0/index=0{AB2AC6E9-D72D-4F0F-8251-5F643F9B4E76}resource=/crio_Mod1/DI8;0;ReadMethodType=bool{AC2806CE-A965-48BF-8C21-64B5C17D41AC}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=bool{AE50F7E2-7756-426D-B141-7D02C6851358}resource=/crio_Mod1/DI5;0;ReadMethodType=bool{AE618064-30BD-4FD8-92F8-516F46545FBD}NumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool{B3C7F845-41F2-446C-8C5C-FE57C9CF43BD}/name=Axis 1.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=6{B3D43C34-FEFB-4D29-A9DC-E6790F105CD2}/name=Axis 1.DIO Status (Port B)/datatype=6/direction=0/index=5{B3F70B56-55F8-4007-B34E-DE323353B34B}/name=Axis 1.Status Register/datatype=6/direction=0/index=1{B40685F8-57B0-463E-954E-158889844CF4}resource=/crio_Mod2/Vsup Voltage;0;ReadMethodType=u16{B52805A6-988A-4800-B3FD-A91EDF13F323}resource=/crio_Mod1/DO13;0;ReadMethodType=bool;WriteMethodType=bool{B6256622-F944-4E75-BCCE-DAF23D644E65}/name=Axis 2.Steps Generated/datatype=2/direction=0/index=10{B9EA829D-B112-451B-A1E0-73B7EC92B72C}resource=/crio_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=bool{BA4668F3-3FB1-47DB-A134-D6649E9F5F72}resource=/crio_Mod3/Phase A Current;0;ReadMethodType=I16{BD3E74EB-6C84-406E-A6CC-0B3AF54DFFC8}resource=/crio_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=bool{BD8B5BF3-B28D-4C14-BC4E-A90CFCE7EAF3}resource=/crio_Mod1/DI12;0;ReadMethodType=bool{BDCB389F-C80C-458B-B7C6-8B848095CF6C}resource=/crio_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=bool{BF74F566-726D-4FC6-9AA1-F751E6293481}resource=/crio_Mod1/DO15;0;ReadMethodType=bool;WriteMethodType=bool{BFC9F9C2-196A-4953-BC5D-FAC0CECDEA1E}resource=/Scan Clock;0;ReadMethodType=bool{C343CFF2-03EE-42A6-B540-56CAE3AC19F6}resource=/crio_Mod3/Vsup Voltage;0;ReadMethodType=u16{C3745DB5-E97B-4E0B-BDD0-6E24EBE686D2}/name=Axis 3.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=20{C5FAB816-19EE-4821-B57F-41707B179D4F}/name=Axis 3.Mailbox (Host to FPGA)/datatype=5/direction=1/index=11{C8B9F7DD-C86F-4681-AF22-A375691DCB63}resource=/crio_Mod3/Phase B Neg;0;WriteMethodType=bool{C9538BF3-4301-4422-84DB-35B49B1312C2}resource=/crio_Mod4/Phase A Current;0;ReadMethodType=I16{CA2DB12E-EF97-4735-811B-F3D08E3AC19E}/name=Axis 2.DIO Status (Port A)/datatype=6/direction=0/index=11{CAF73AA4-6484-4F0F-BF41-F0C0EC242404}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=bool{CBE28825-E761-45A3-9161-325B01DEDB16}/name=Axis 1.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=2{D14BE3AE-4EFB-46E7-B0D7-0733C33D8736}/name=Axis 3.Control Register/datatype=6/direction=1/index=10{D29EB5F7-9DC9-4D9F-914A-F49B8E1608DD}resource=/crio_Mod1/DO14;0;ReadMethodType=bool;WriteMethodType=bool{D386CC9B-0765-4A90-8BD4-0E243C679022}resource=/crio_Mod2/Phase A Current;0;ReadMethodType=I16{D4B69A68-9DA8-4377-80B2-81EEBCBE57CA}resource=/crio_Mod1/DI15:8;0;ReadMethodType=u8{DA0FAA92-F7C0-45E7-958B-A2F6111D9869}/name=Axis 2.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=13{DC27D298-593E-4DE3-ADE3-28D6C6870856}/name=Axis 1.Position Feedback/datatype=2/direction=0/index=2{DC670331-7130-4FF8-ACD8-BB5DF85B84A1}/name=Axis 3.Status Register/datatype=6/direction=0/index=15{DFF36B8A-13B2-4D63-BF19-4BDAFB5DCD62}resource=/crio_Mod1/DI0;0;ReadMethodType=bool{E086F6D2-DAA0-4963-A0DB-2BCCDD71A8D5}resource=/crio_Mod1/DI9;0;ReadMethodType=bool{E25E9851-D552-488C-9B8D-D63A0813CB0D}resource=/crio_Mod4/Phase B Pos;0;WriteMethodType=bool{E302B98A-C798-4E3F-A53A-04207F12B35C}resource=/crio_Mod1/DI3;0;ReadMethodType=bool{E44C817D-F6DC-401A-85D4-F3B9ACD9A4D2}/name=Axis 2.Status Register/datatype=6/direction=0/index=8{E5EA3F98-15FB-40B1-B0A9-8249BCE88E60}resource=/crio_Mod1/DI4;0;ReadMethodType=bool{E65CB17F-1DF1-434E-A64E-FC1FC266D84C}resource=/crio_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=bool{E6B28907-AA6D-4D8C-BED7-04C972B9C9FF}/name=Axis 3.DIO Status (Port A)/datatype=6/direction=0/index=18{F0C3C458-8A6C-438A-BB19-5D62B28B9211}resource=/Reset RT App;0;WriteMethodType=bool{F2BCE5E9-4F44-40C2-BE47-22E1C137C971}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9503[crioConfig.End]{F6425AA6-51D3-4206-B29C-5B86EA0DE4A8}/name=Axis 3.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=12{F7C8008D-3B46-48F7-AB2C-528ADDD8D52A}resource=/crio_Mod1/DI6;0;ReadMethodType=bool{F7CCBC1F-27FF-4FAA-9C46-758896B69811}NumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=bool{F91B61AF-21BC-4BDF-A274-7CCB5F1F10E0}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{FC7B3B80-6ECB-4D16-A0EC-640A0D4FBC59}resource=/crio_Mod1/DI1;0;ReadMethodType=bool{FCE1384A-2AF9-40C5-96ED-46C5D20550DA}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=bool{FEDB15D4-5B84-4052-B9F6-F6AB023BC552}resource=/crio_Mod4/User LED;0;WriteMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">10 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool12.8 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool13.1072 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Axis 1.Control Register/name=Axis 1.Control Register/datatype=6/direction=1/index=0Axis 1.DIO Control (Port A)/name=Axis 1.DIO Control (Port A)/datatype=6/direction=1/index=3Axis 1.DIO Control (Port B)/name=Axis 1.DIO Control (Port B)/datatype=6/direction=1/index=4Axis 1.DIO Status (Port A)/name=Axis 1.DIO Status (Port A)/datatype=6/direction=0/index=4Axis 1.DIO Status (Port B)/name=Axis 1.DIO Status (Port B)/datatype=6/direction=0/index=5Axis 1.Mailbox (FPGA to Host)/name=Axis 1.Mailbox (FPGA to Host)/datatype=5/direction=0/index=0Axis 1.Mailbox (Host to FPGA)/name=Axis 1.Mailbox (Host to FPGA)/datatype=5/direction=1/index=1Axis 1.Position Feedback/name=Axis 1.Position Feedback/datatype=2/direction=0/index=2Axis 1.Position Setpoint/name=Axis 1.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=2Axis 1.Status Register/name=Axis 1.Status Register/datatype=6/direction=0/index=1Axis 1.Steps Generated/name=Axis 1.Steps Generated/datatype=2/direction=0/index=3Axis 1.Velocity Feedback/name=Axis 1.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=6Axis 2.Control Register/name=Axis 2.Control Register/datatype=6/direction=1/index=5Axis 2.DIO Control (Port A)/name=Axis 2.DIO Control (Port A)/datatype=6/direction=1/index=8Axis 2.DIO Control (Port B)/name=Axis 2.DIO Control (Port B)/datatype=6/direction=1/index=9Axis 2.DIO Status (Port A)/name=Axis 2.DIO Status (Port A)/datatype=6/direction=0/index=11Axis 2.DIO Status (Port B)/name=Axis 2.DIO Status (Port B)/datatype=6/direction=0/index=12Axis 2.Mailbox (FPGA to Host)/name=Axis 2.Mailbox (FPGA to Host)/datatype=5/direction=0/index=7Axis 2.Mailbox (Host to FPGA)/name=Axis 2.Mailbox (Host to FPGA)/datatype=5/direction=1/index=6Axis 2.Position Feedback/name=Axis 2.Position Feedback/datatype=2/direction=0/index=9Axis 2.Position Setpoint/name=Axis 2.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=7Axis 2.Status Register/name=Axis 2.Status Register/datatype=6/direction=0/index=8Axis 2.Steps Generated/name=Axis 2.Steps Generated/datatype=2/direction=0/index=10Axis 2.Velocity Feedback/name=Axis 2.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=13Axis 3.Control Register/name=Axis 3.Control Register/datatype=6/direction=1/index=10Axis 3.DIO Control (Port A)/name=Axis 3.DIO Control (Port A)/datatype=6/direction=1/index=13Axis 3.DIO Control (Port B)/name=Axis 3.DIO Control (Port B)/datatype=6/direction=1/index=14Axis 3.DIO Status (Port A)/name=Axis 3.DIO Status (Port A)/datatype=6/direction=0/index=18Axis 3.DIO Status (Port B)/name=Axis 3.DIO Status (Port B)/datatype=6/direction=0/index=19Axis 3.Mailbox (FPGA to Host)/name=Axis 3.Mailbox (FPGA to Host)/datatype=5/direction=0/index=14Axis 3.Mailbox (Host to FPGA)/name=Axis 3.Mailbox (Host to FPGA)/datatype=5/direction=1/index=11Axis 3.Position Feedback/name=Axis 3.Position Feedback/datatype=2/direction=0/index=16Axis 3.Position Setpoint/name=Axis 3.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=12Axis 3.Status Register/name=Axis 3.Status Register/datatype=6/direction=0/index=15Axis 3.Steps Generated/name=Axis 3.Steps Generated/datatype=2/direction=0/index=17Axis 3.Velocity Feedback/name=Axis 3.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=20Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO_Trig0ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig1ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig2ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig3ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig4NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=boolcRIO_Trig5NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=boolcRIO_Trig6NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=boolcRIO_Trig7NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Mod1/DI0resource=/crio_Mod1/DI0;0;ReadMethodType=boolMod1/DI10resource=/crio_Mod1/DI10;0;ReadMethodType=boolMod1/DI11resource=/crio_Mod1/DI11;0;ReadMethodType=boolMod1/DI12resource=/crio_Mod1/DI12;0;ReadMethodType=boolMod1/DI13resource=/crio_Mod1/DI13;0;ReadMethodType=boolMod1/DI14resource=/crio_Mod1/DI14;0;ReadMethodType=boolMod1/DI15:0resource=/crio_Mod1/DI15:0;0;ReadMethodType=u16Mod1/DI15:8resource=/crio_Mod1/DI15:8;0;ReadMethodType=u8Mod1/DI15resource=/crio_Mod1/DI15;0;ReadMethodType=boolMod1/DI1resource=/crio_Mod1/DI1;0;ReadMethodType=boolMod1/DI2resource=/crio_Mod1/DI2;0;ReadMethodType=boolMod1/DI3resource=/crio_Mod1/DI3;0;ReadMethodType=boolMod1/DI4resource=/crio_Mod1/DI4;0;ReadMethodType=boolMod1/DI5resource=/crio_Mod1/DI5;0;ReadMethodType=boolMod1/DI6resource=/crio_Mod1/DI6;0;ReadMethodType=boolMod1/DI7:0resource=/crio_Mod1/DI7:0;0;ReadMethodType=u8Mod1/DI7resource=/crio_Mod1/DI7;0;ReadMethodType=boolMod1/DI8resource=/crio_Mod1/DI8;0;ReadMethodType=boolMod1/DI9resource=/crio_Mod1/DI9;0;ReadMethodType=boolMod1/DO0resource=/crio_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO10resource=/crio_Mod1/DO10;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO11resource=/crio_Mod1/DO11;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO12resource=/crio_Mod1/DO12;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO13resource=/crio_Mod1/DO13;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO14resource=/crio_Mod1/DO14;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO15:0resource=/crio_Mod1/DO15:0;0;ReadMethodType=u16;WriteMethodType=u16Mod1/DO15:8resource=/crio_Mod1/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DO15resource=/crio_Mod1/DO15;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO1resource=/crio_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO2resource=/crio_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO3resource=/crio_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO4resource=/crio_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO5resource=/crio_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO6resource=/crio_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO7:0resource=/crio_Mod1/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DO7resource=/crio_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO8resource=/crio_Mod1/DO8;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO9resource=/crio_Mod1/DO9;0;ReadMethodType=bool;WriteMethodType=boolMod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9375,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod2/Phase A Currentresource=/crio_Mod2/Phase A Current;0;ReadMethodType=I16Mod2/Phase A Negresource=/crio_Mod2/Phase A Neg;0;WriteMethodType=boolMod2/Phase A Posresource=/crio_Mod2/Phase A Pos;0;WriteMethodType=boolMod2/Phase B Currentresource=/crio_Mod2/Phase B Current;0;ReadMethodType=I16Mod2/Phase B Negresource=/crio_Mod2/Phase B Neg;0;WriteMethodType=boolMod2/Phase B Posresource=/crio_Mod2/Phase B Pos;0;WriteMethodType=boolMod2/User LEDresource=/crio_Mod2/User LED;0;WriteMethodType=boolMod2/Vsup Voltageresource=/crio_Mod2/Vsup Voltage;0;ReadMethodType=u16Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9503[crioConfig.End]Mod3/Phase A Currentresource=/crio_Mod3/Phase A Current;0;ReadMethodType=I16Mod3/Phase A Negresource=/crio_Mod3/Phase A Neg;0;WriteMethodType=boolMod3/Phase A Posresource=/crio_Mod3/Phase A Pos;0;WriteMethodType=boolMod3/Phase B Currentresource=/crio_Mod3/Phase B Current;0;ReadMethodType=I16Mod3/Phase B Negresource=/crio_Mod3/Phase B Neg;0;WriteMethodType=boolMod3/Phase B Posresource=/crio_Mod3/Phase B Pos;0;WriteMethodType=boolMod3/User LEDresource=/crio_Mod3/User LED;0;WriteMethodType=boolMod3/Vsup Voltageresource=/crio_Mod3/Vsup Voltage;0;ReadMethodType=u16Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9503[crioConfig.End]Mod4/Phase A Currentresource=/crio_Mod4/Phase A Current;0;ReadMethodType=I16Mod4/Phase A Negresource=/crio_Mod4/Phase A Neg;0;WriteMethodType=boolMod4/Phase A Posresource=/crio_Mod4/Phase A Pos;0;WriteMethodType=boolMod4/Phase B Currentresource=/crio_Mod4/Phase B Current;0;ReadMethodType=I16Mod4/Phase B Negresource=/crio_Mod4/Phase B Neg;0;WriteMethodType=boolMod4/Phase B Posresource=/crio_Mod4/Phase B Pos;0;WriteMethodType=boolMod4/User LEDresource=/crio_Mod4/User LED;0;WriteMethodType=boolMod4/Vsup Voltageresource=/crio_Mod4/Vsup Voltage;0;ReadMethodType=u16Mod4[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9503[crioConfig.End]Offset from Time Reference ValidNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=boolOffset from Time ReferenceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32Reset RT Appresource=/Reset RT App;0;WriteMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolSystem Watchdog ExpiredNumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=boolTime SourceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctlTime Synchronization FaultNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=boolTimeNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64USER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool</Property>
				<Property Name="Mode" Type="Int">0</Property>
				<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">cRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
				<Property Name="Resource Name" Type="Str">RIO0</Property>
				<Property Name="Target Class" Type="Str">cRIO-9056</Property>
				<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
				<Item Name="Chassis I/O" Type="Folder">
					<Item Name="Chassis Temperature" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Chassis Temperature</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1B64D359-A52B-4F6E-84B9-E2C60FBBF03B}</Property>
					</Item>
					<Item Name="Sleep" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Sleep</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{05A81A96-A932-488D-9473-888AF8C3AB12}</Property>
					</Item>
					<Item Name="System Reset" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/System Reset</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{131E4BA5-61B4-4357-971A-4160B95447E5}</Property>
					</Item>
					<Item Name="Scan Clock" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Scan Clock</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BFC9F9C2-196A-4953-BC5D-FAC0CECDEA1E}</Property>
					</Item>
					<Item Name="Reset RT App" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Reset RT App</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F0C3C458-8A6C-438A-BB19-5D62B28B9211}</Property>
					</Item>
					<Item Name="System Watchdog Expired" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/System Watchdog Expired</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F7CCBC1F-27FF-4FAA-9C46-758896B69811}</Property>
					</Item>
					<Item Name="12.8 MHz Timebase" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/12.8 MHz Timebase</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{36639CFF-6AB6-40BD-B425-DA84855425D7}</Property>
					</Item>
					<Item Name="13.1072 MHz Timebase" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/13.1072 MHz Timebase</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8845D70A-3D8B-49E0-AEA5-EE3B8141E438}</Property>
					</Item>
					<Item Name="10 MHz Timebase" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/10 MHz Timebase</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AE618064-30BD-4FD8-92F8-516F46545FBD}</Property>
					</Item>
					<Item Name="USER FPGA LED" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/USER FPGA LED</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F91B61AF-21BC-4BDF-A274-7CCB5F1F10E0}</Property>
					</Item>
				</Item>
				<Item Name="cRIO_Trig" Type="Folder">
					<Item Name="cRIO_Trig0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5C0C0298-27DB-474F-89DA-9CCEBB00E7B5}</Property>
					</Item>
					<Item Name="cRIO_Trig1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CAF73AA4-6484-4F0F-BF41-F0C0EC242404}</Property>
					</Item>
					<Item Name="cRIO_Trig2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4A0A5AD1-CC49-4854-9036-9503E724C780}</Property>
					</Item>
					<Item Name="cRIO_Trig3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{514991AE-3F66-447E-8071-01006692529D}</Property>
					</Item>
					<Item Name="cRIO_Trig4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6D91CD44-DB04-47C3-A62C-16857B5AA964}</Property>
					</Item>
					<Item Name="cRIO_Trig5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{047258CE-911D-42C2-A57D-DE27BB9E6FEC}</Property>
					</Item>
					<Item Name="cRIO_Trig6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AC2806CE-A965-48BF-8C21-64B5C17D41AC}</Property>
					</Item>
					<Item Name="cRIO_Trig7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{13433233-B1E3-41EE-843C-6F079D8DA1CE}</Property>
					</Item>
				</Item>
				<Item Name="Time Synchronization" Type="Folder">
					<Item Name="Time" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Time</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{09AB7A74-FAD4-439D-B637-9EF3ADAA2D7E}</Property>
					</Item>
					<Item Name="Time Source" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Time Source</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9F5B802E-B199-494A-AFCA-E189C0C34B3B}</Property>
					</Item>
					<Item Name="Time Synchronization Fault" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Time Synchronization Fault</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FCE1384A-2AF9-40C5-96ED-46C5D20550DA}</Property>
					</Item>
					<Item Name="Offset from Time Reference" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Offset from Time Reference</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2DC7761A-7739-4BFC-982B-7E45F47099A2}</Property>
					</Item>
					<Item Name="Offset from Time Reference Valid" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Offset from Time Reference Valid</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A0AFFE3F-BCDE-458A-A2FB-6F87F5424577}</Property>
					</Item>
				</Item>
				<Item Name="Mod1" Type="Folder">
					<Item Name="Mod1/DI0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DFF36B8A-13B2-4D63-BF19-4BDAFB5DCD62}</Property>
					</Item>
					<Item Name="Mod1/DI1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FC7B3B80-6ECB-4D16-A0EC-640A0D4FBC59}</Property>
					</Item>
					<Item Name="Mod1/DI2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{59CC0C88-22A1-4840-A476-5EC74AD59EEE}</Property>
					</Item>
					<Item Name="Mod1/DI3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E302B98A-C798-4E3F-A53A-04207F12B35C}</Property>
					</Item>
					<Item Name="Mod1/DI4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E5EA3F98-15FB-40B1-B0A9-8249BCE88E60}</Property>
					</Item>
					<Item Name="Mod1/DI5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AE50F7E2-7756-426D-B141-7D02C6851358}</Property>
					</Item>
					<Item Name="Mod1/DI6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F7C8008D-3B46-48F7-AB2C-528ADDD8D52A}</Property>
					</Item>
					<Item Name="Mod1/DI7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1731DCD7-A5EC-4931-9123-1CE53C50FCB3}</Property>
					</Item>
					<Item Name="Mod1/DI8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AB2AC6E9-D72D-4F0F-8251-5F643F9B4E76}</Property>
					</Item>
					<Item Name="Mod1/DI9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E086F6D2-DAA0-4963-A0DB-2BCCDD71A8D5}</Property>
					</Item>
					<Item Name="Mod1/DI10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{66B47596-5DE0-43D1-B697-174EA01F359B}</Property>
					</Item>
					<Item Name="Mod1/DI11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{88657F19-2BA2-492A-9398-45300D6B8A81}</Property>
					</Item>
					<Item Name="Mod1/DI12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BD8B5BF3-B28D-4C14-BC4E-A90CFCE7EAF3}</Property>
					</Item>
					<Item Name="Mod1/DI13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{15176B56-6AE9-44B1-B312-A218C64C0A3E}</Property>
					</Item>
					<Item Name="Mod1/DI14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0017E7D9-AF68-4A4D-BDCB-E0F80A595821}</Property>
					</Item>
					<Item Name="Mod1/DI15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{92F4FB54-4EEB-4036-A6FB-964EFB5CE42D}</Property>
					</Item>
					<Item Name="Mod1/DI7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{697EF40C-5290-4619-AEE3-4E63DDA449D0}</Property>
					</Item>
					<Item Name="Mod1/DI15:8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D4B69A68-9DA8-4377-80B2-81EEBCBE57CA}</Property>
					</Item>
					<Item Name="Mod1/DI15:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DI15:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{12F80F9D-1A19-493C-A636-E7CF8033A12E}</Property>
					</Item>
					<Item Name="Mod1/DO0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E65CB17F-1DF1-434E-A64E-FC1FC266D84C}</Property>
					</Item>
					<Item Name="Mod1/DO1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BDCB389F-C80C-458B-B7C6-8B848095CF6C}</Property>
					</Item>
					<Item Name="Mod1/DO2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9C2165FA-8616-48F6-B956-1A396BED9378}</Property>
					</Item>
					<Item Name="Mod1/DO3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{18DCE8CD-C41D-4393-9EF9-0068DAF3E454}</Property>
					</Item>
					<Item Name="Mod1/DO4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6CD02D79-DFB0-4E13-B8B0-4BF96F177BEF}</Property>
					</Item>
					<Item Name="Mod1/DO5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BD3E74EB-6C84-406E-A6CC-0B3AF54DFFC8}</Property>
					</Item>
					<Item Name="Mod1/DO6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B9EA829D-B112-451B-A1E0-73B7EC92B72C}</Property>
					</Item>
					<Item Name="Mod1/DO7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{37B9D1F4-8948-4C7D-859E-840CB3C009E8}</Property>
					</Item>
					<Item Name="Mod1/DO8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6E3BA9CA-2C9B-4CB9-B653-CD2595A217C6}</Property>
					</Item>
					<Item Name="Mod1/DO9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{196A8036-32AD-4C7A-B6FD-2089C426A26F}</Property>
					</Item>
					<Item Name="Mod1/DO10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7F9F9F36-C1DE-4CF2-AABE-88FF5FFD8204}</Property>
					</Item>
					<Item Name="Mod1/DO11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6A747376-939C-40E7-BDB1-CBEA97D3007B}</Property>
					</Item>
					<Item Name="Mod1/DO12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7D03D325-9E75-4C28-B764-31C7691A4DDF}</Property>
					</Item>
					<Item Name="Mod1/DO13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B52805A6-988A-4800-B3FD-A91EDF13F323}</Property>
					</Item>
					<Item Name="Mod1/DO14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D29EB5F7-9DC9-4D9F-914A-F49B8E1608DD}</Property>
					</Item>
					<Item Name="Mod1/DO15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BF74F566-726D-4FC6-9AA1-F751E6293481}</Property>
					</Item>
					<Item Name="Mod1/DO7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{812C894E-0D98-4649-819A-AEA3ED0D8FB3}</Property>
					</Item>
					<Item Name="Mod1/DO15:8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{89ADC391-1BC0-4EB7-9C7B-BEF6E0B4E2DE}</Property>
					</Item>
					<Item Name="Mod1/DO15:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DO15:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1E4D6A2C-5B1C-4B24-8F0F-D0DE4861E24C}</Property>
					</Item>
				</Item>
				<Item Name="Mod2" Type="Folder">
					<Item Name="Mod2/Phase A Pos" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/Phase A Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9E0BFAF6-29A4-4BDA-8BB0-50EC671080B5}</Property>
					</Item>
					<Item Name="Mod2/Phase A Neg" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/Phase A Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0D6D93D5-D434-4FF8-8977-5D8277810B89}</Property>
					</Item>
					<Item Name="Mod2/Phase B Pos" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/Phase B Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{29E50DF6-D6C6-4E70-8683-467F7D2BA535}</Property>
					</Item>
					<Item Name="Mod2/Phase B Neg" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/Phase B Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{822F4B12-3504-4AB6-91CA-915DF3D744C7}</Property>
					</Item>
					<Item Name="Mod2/Phase A Current" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/Phase A Current</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D386CC9B-0765-4A90-8BD4-0E243C679022}</Property>
					</Item>
					<Item Name="Mod2/Phase B Current" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/Phase B Current</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{311CCA04-9C8B-47AE-9291-2427298E19EF}</Property>
					</Item>
					<Item Name="Mod2/Vsup Voltage" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/Vsup Voltage</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B40685F8-57B0-463E-954E-158889844CF4}</Property>
					</Item>
					<Item Name="Mod2/User LED" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/User LED</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{22F14F7B-7878-4735-8FFF-6E208B901DCB}</Property>
					</Item>
				</Item>
				<Item Name="Mod3" Type="Folder">
					<Item Name="Mod3/Phase A Pos" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/Phase A Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9C71592D-D581-44ED-9946-F12827544A5C}</Property>
					</Item>
					<Item Name="Mod3/Phase A Neg" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/Phase A Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A0DCCCBC-384C-4A97-8439-C5B775CF3C25}</Property>
					</Item>
					<Item Name="Mod3/Phase B Pos" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/Phase B Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1CBD5F71-337D-444F-A614-F4E3D2FC5266}</Property>
					</Item>
					<Item Name="Mod3/Phase B Neg" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/Phase B Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C8B9F7DD-C86F-4681-AF22-A375691DCB63}</Property>
					</Item>
					<Item Name="Mod3/Phase A Current" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/Phase A Current</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BA4668F3-3FB1-47DB-A134-D6649E9F5F72}</Property>
					</Item>
					<Item Name="Mod3/Phase B Current" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/Phase B Current</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1F296AF1-93A2-4B75-9EE0-63DE3E43B3CD}</Property>
					</Item>
					<Item Name="Mod3/Vsup Voltage" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/Vsup Voltage</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C343CFF2-03EE-42A6-B540-56CAE3AC19F6}</Property>
					</Item>
					<Item Name="Mod3/User LED" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/User LED</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0CF72225-8BCB-413F-AF38-51EA4654D1B6}</Property>
					</Item>
				</Item>
				<Item Name="Mod4" Type="Folder">
					<Item Name="Mod4/Phase A Pos" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/Phase A Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5B08D5E4-F4FB-4A3B-85A6-B54AE2A3CB8F}</Property>
					</Item>
					<Item Name="Mod4/Phase A Neg" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/Phase A Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2A7D546E-C337-47B4-86F2-EC01EB73A8D4}</Property>
					</Item>
					<Item Name="Mod4/Phase B Pos" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/Phase B Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E25E9851-D552-488C-9B8D-D63A0813CB0D}</Property>
					</Item>
					<Item Name="Mod4/Phase B Neg" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/Phase B Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7BBA44A1-0B1C-4D3B-8C34-77E5F8BFF19D}</Property>
					</Item>
					<Item Name="Mod4/Phase A Current" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/Phase A Current</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C9538BF3-4301-4422-84DB-35B49B1312C2}</Property>
					</Item>
					<Item Name="Mod4/Phase B Current" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/Phase B Current</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6F7A0866-03D4-4F01-BD82-CFD8BEFAFF79}</Property>
					</Item>
					<Item Name="Mod4/Vsup Voltage" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/Vsup Voltage</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{36A125B8-5220-4C6B-B2FC-0B3C2F2703F1}</Property>
					</Item>
					<Item Name="Mod4/User LED" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/User LED</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FEDB15D4-5B84-4052-B9F6-F6AB023BC552}</Property>
					</Item>
				</Item>
				<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
					<Property Name="FPGA.PersistentID" Type="Str">{34F7A81C-BA4C-400F-9078-70CD2104C790}</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
					<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
				</Item>
				<Item Name="IP Builder" Type="IP Builder Target">
					<Item Name="Dependencies" Type="Dependencies"/>
					<Item Name="Build Specifications" Type="Build"/>
				</Item>
				<Item Name="Mod1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9375</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{01ADC950-F697-4E10-AC0F-3597AE707FCD}</Property>
				</Item>
				<Item Name="Mod2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9503</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6FBB5A63-EA42-449D-BBFE-CC2C51A9EAD7}</Property>
				</Item>
				<Item Name="Mod3" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 3</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9503</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9DA89C10-2764-4628-9BFD-833BB10392D5}</Property>
				</Item>
				<Item Name="Mod4" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 4</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9503</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F2BCE5E9-4F44-40C2-BE47-22E1C137C971}</Property>
				</Item>
				<Item Name="FPGA.vi" Type="VI" URL="../FPGA/FPGA.vi">
					<Property Name="BuildSpec" Type="Str">{15485CA1-FD5A-46BA-9932-E7686A24945B}</Property>
					<Property Name="configString.guid" Type="Str">{0017E7D9-AF68-4A4D-BDCB-E0F80A595821}resource=/crio_Mod1/DI14;0;ReadMethodType=bool{01ADC950-F697-4E10-AC0F-3597AE707FCD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9375,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0337EAE1-9570-4F55-9DC5-6B925C4780EE}/name=Axis 2.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=7{03630C58-CD97-4F3E-8BE6-E29784853061}/name=Axis 1.DIO Control (Port B)/datatype=6/direction=1/index=4{047258CE-911D-42C2-A57D-DE27BB9E6FEC}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=bool{05A81A96-A932-488D-9473-888AF8C3AB12}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{09AB7A74-FAD4-439D-B637-9EF3ADAA2D7E}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64{0CF72225-8BCB-413F-AF38-51EA4654D1B6}resource=/crio_Mod3/User LED;0;WriteMethodType=bool{0D6D93D5-D434-4FF8-8977-5D8277810B89}resource=/crio_Mod2/Phase A Neg;0;WriteMethodType=bool{0DF8051B-C1F4-4EB5-898D-AF590BD87F25}/name=Axis 2.DIO Control (Port A)/datatype=6/direction=1/index=8{12F80F9D-1A19-493C-A636-E7CF8033A12E}resource=/crio_Mod1/DI15:0;0;ReadMethodType=u16{131E4BA5-61B4-4357-971A-4160B95447E5}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{13433233-B1E3-41EE-843C-6F079D8DA1CE}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=bool{15176B56-6AE9-44B1-B312-A218C64C0A3E}resource=/crio_Mod1/DI13;0;ReadMethodType=bool{1731DCD7-A5EC-4931-9123-1CE53C50FCB3}resource=/crio_Mod1/DI7;0;ReadMethodType=bool{18DCE8CD-C41D-4393-9EF9-0068DAF3E454}resource=/crio_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=bool{196A8036-32AD-4C7A-B6FD-2089C426A26F}resource=/crio_Mod1/DO9;0;ReadMethodType=bool;WriteMethodType=bool{1B64D359-A52B-4F6E-84B9-E2C60FBBF03B}resource=/Chassis Temperature;0;ReadMethodType=i16{1CBD5F71-337D-444F-A614-F4E3D2FC5266}resource=/crio_Mod3/Phase B Pos;0;WriteMethodType=bool{1E4D6A2C-5B1C-4B24-8F0F-D0DE4861E24C}resource=/crio_Mod1/DO15:0;0;ReadMethodType=u16;WriteMethodType=u16{1F296AF1-93A2-4B75-9EE0-63DE3E43B3CD}resource=/crio_Mod3/Phase B Current;0;ReadMethodType=I16{22F14F7B-7878-4735-8FFF-6E208B901DCB}resource=/crio_Mod2/User LED;0;WriteMethodType=bool{29E50DF6-D6C6-4E70-8683-467F7D2BA535}resource=/crio_Mod2/Phase B Pos;0;WriteMethodType=bool{2A7D546E-C337-47B4-86F2-EC01EB73A8D4}resource=/crio_Mod4/Phase A Neg;0;WriteMethodType=bool{2DC7761A-7739-4BFC-982B-7E45F47099A2}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32{2DE03463-12E0-48A1-B408-73CC91ED7D0A}/name=Axis 2.Position Feedback/datatype=2/direction=0/index=9{30AA07C1-223C-4AB8-A1A9-DB4EAE61F437}/name=Axis 3.DIO Control (Port A)/datatype=6/direction=1/index=13{311CCA04-9C8B-47AE-9291-2427298E19EF}resource=/crio_Mod2/Phase B Current;0;ReadMethodType=I16{33EE390F-78F9-400C-B704-58C0D7A04188}/name=Axis 2.Mailbox (Host to FPGA)/datatype=5/direction=1/index=6{34F7A81C-BA4C-400F-9078-70CD2104C790}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{36639CFF-6AB6-40BD-B425-DA84855425D7}NumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool{36A125B8-5220-4C6B-B2FC-0B3C2F2703F1}resource=/crio_Mod4/Vsup Voltage;0;ReadMethodType=u16{37B9D1F4-8948-4C7D-859E-840CB3C009E8}resource=/crio_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=bool{407E808C-C7FA-4964-9B71-E150D4214690}/name=Axis 1.Mailbox (Host to FPGA)/datatype=5/direction=1/index=1{4281C939-6849-4542-8375-DC854108F893}/name=Axis 3.DIO Status (Port B)/datatype=6/direction=0/index=19{4813C9AE-1626-40E2-A2D1-92C893C9653A}/name=Axis 2.Control Register/datatype=6/direction=1/index=5{4A0A5AD1-CC49-4854-9036-9503E724C780}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=bool{4A50DE0C-31A7-40D3-AFC5-1226FDA68401}/name=Axis 2.Mailbox (FPGA to Host)/datatype=5/direction=0/index=7{514991AE-3F66-447E-8071-01006692529D}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=bool{59CC0C88-22A1-4840-A476-5EC74AD59EEE}resource=/crio_Mod1/DI2;0;ReadMethodType=bool{59F18F21-D34A-440E-BF0C-DADCE1F6BEE6}/name=Axis 1.DIO Status (Port A)/datatype=6/direction=0/index=4{59F7A552-8DE9-40A7-BF40-124E0354177F}/name=Axis 3.Mailbox (FPGA to Host)/datatype=5/direction=0/index=14{5B08D5E4-F4FB-4A3B-85A6-B54AE2A3CB8F}resource=/crio_Mod4/Phase A Pos;0;WriteMethodType=bool{5C0C0298-27DB-474F-89DA-9CCEBB00E7B5}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=bool{5E79850A-008E-435E-8EC3-FC448F4B7752}/name=Axis 3.Steps Generated/datatype=2/direction=0/index=17{63CAEC4C-4375-4E8B-8D45-9BD560A34959}/name=Axis 1.DIO Control (Port A)/datatype=6/direction=1/index=3{66B47596-5DE0-43D1-B697-174EA01F359B}resource=/crio_Mod1/DI10;0;ReadMethodType=bool{697EF40C-5290-4619-AEE3-4E63DDA449D0}resource=/crio_Mod1/DI7:0;0;ReadMethodType=u8{6A747376-939C-40E7-BDB1-CBEA97D3007B}resource=/crio_Mod1/DO11;0;ReadMethodType=bool;WriteMethodType=bool{6CD02D79-DFB0-4E13-B8B0-4BF96F177BEF}resource=/crio_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=bool{6D91CD44-DB04-47C3-A62C-16857B5AA964}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=bool{6E3BA9CA-2C9B-4CB9-B653-CD2595A217C6}resource=/crio_Mod1/DO8;0;ReadMethodType=bool;WriteMethodType=bool{6F7A0866-03D4-4F01-BD82-CFD8BEFAFF79}resource=/crio_Mod4/Phase B Current;0;ReadMethodType=I16{6FBB5A63-EA42-449D-BBFE-CC2C51A9EAD7}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9503[crioConfig.End]{79BC5C5E-9EFF-4453-8DEC-800FCBFA2543}/name=Axis 3.DIO Control (Port B)/datatype=6/direction=1/index=14{7BBA44A1-0B1C-4D3B-8C34-77E5F8BFF19D}resource=/crio_Mod4/Phase B Neg;0;WriteMethodType=bool{7D03D325-9E75-4C28-B764-31C7691A4DDF}resource=/crio_Mod1/DO12;0;ReadMethodType=bool;WriteMethodType=bool{7F9F9F36-C1DE-4CF2-AABE-88FF5FFD8204}resource=/crio_Mod1/DO10;0;ReadMethodType=bool;WriteMethodType=bool{812C894E-0D98-4649-819A-AEA3ED0D8FB3}resource=/crio_Mod1/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{822F4B12-3504-4AB6-91CA-915DF3D744C7}resource=/crio_Mod2/Phase B Neg;0;WriteMethodType=bool{854317AC-436F-4D15-B272-1B28B36E5683}/name=Axis 1.Steps Generated/datatype=2/direction=0/index=3{8845D70A-3D8B-49E0-AEA5-EE3B8141E438}NumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool{88657F19-2BA2-492A-9398-45300D6B8A81}resource=/crio_Mod1/DI11;0;ReadMethodType=bool{89ADC391-1BC0-4EB7-9C7B-BEF6E0B4E2DE}resource=/crio_Mod1/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{92F4FB54-4EEB-4036-A6FB-964EFB5CE42D}resource=/crio_Mod1/DI15;0;ReadMethodType=bool{936546CE-563B-48D1-A9F9-78D46743E3C7}/name=Axis 1.Control Register/datatype=6/direction=1/index=0{9AD3274D-E096-44FC-808D-806587084876}/name=Axis 2.DIO Control (Port B)/datatype=6/direction=1/index=9{9C2165FA-8616-48F6-B956-1A396BED9378}resource=/crio_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=bool{9C71592D-D581-44ED-9946-F12827544A5C}resource=/crio_Mod3/Phase A Pos;0;WriteMethodType=bool{9DA89C10-2764-4628-9BFD-833BB10392D5}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9503[crioConfig.End]{9E0BFAF6-29A4-4BDA-8BB0-50EC671080B5}resource=/crio_Mod2/Phase A Pos;0;WriteMethodType=bool{9F5B802E-B199-494A-AFCA-E189C0C34B3B}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctl{9F67DCC6-8812-4A91-93DC-A6B351C8DC37}/name=Axis 2.DIO Status (Port B)/datatype=6/direction=0/index=12{A0AFFE3F-BCDE-458A-A2FB-6F87F5424577}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=bool{A0DCCCBC-384C-4A97-8439-C5B775CF3C25}resource=/crio_Mod3/Phase A Neg;0;WriteMethodType=bool{A0E4D4FD-3884-43CF-86A1-CCA8C2079A1B}/name=Axis 3.Position Feedback/datatype=2/direction=0/index=16{A6F7D6E6-D7C0-4797-90A0-440D9499B717}/name=Axis 1.Mailbox (FPGA to Host)/datatype=5/direction=0/index=0{AB2AC6E9-D72D-4F0F-8251-5F643F9B4E76}resource=/crio_Mod1/DI8;0;ReadMethodType=bool{AC2806CE-A965-48BF-8C21-64B5C17D41AC}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=bool{AE50F7E2-7756-426D-B141-7D02C6851358}resource=/crio_Mod1/DI5;0;ReadMethodType=bool{AE618064-30BD-4FD8-92F8-516F46545FBD}NumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool{B3C7F845-41F2-446C-8C5C-FE57C9CF43BD}/name=Axis 1.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=6{B3D43C34-FEFB-4D29-A9DC-E6790F105CD2}/name=Axis 1.DIO Status (Port B)/datatype=6/direction=0/index=5{B3F70B56-55F8-4007-B34E-DE323353B34B}/name=Axis 1.Status Register/datatype=6/direction=0/index=1{B40685F8-57B0-463E-954E-158889844CF4}resource=/crio_Mod2/Vsup Voltage;0;ReadMethodType=u16{B52805A6-988A-4800-B3FD-A91EDF13F323}resource=/crio_Mod1/DO13;0;ReadMethodType=bool;WriteMethodType=bool{B6256622-F944-4E75-BCCE-DAF23D644E65}/name=Axis 2.Steps Generated/datatype=2/direction=0/index=10{B9EA829D-B112-451B-A1E0-73B7EC92B72C}resource=/crio_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=bool{BA4668F3-3FB1-47DB-A134-D6649E9F5F72}resource=/crio_Mod3/Phase A Current;0;ReadMethodType=I16{BD3E74EB-6C84-406E-A6CC-0B3AF54DFFC8}resource=/crio_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=bool{BD8B5BF3-B28D-4C14-BC4E-A90CFCE7EAF3}resource=/crio_Mod1/DI12;0;ReadMethodType=bool{BDCB389F-C80C-458B-B7C6-8B848095CF6C}resource=/crio_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=bool{BF74F566-726D-4FC6-9AA1-F751E6293481}resource=/crio_Mod1/DO15;0;ReadMethodType=bool;WriteMethodType=bool{BFC9F9C2-196A-4953-BC5D-FAC0CECDEA1E}resource=/Scan Clock;0;ReadMethodType=bool{C343CFF2-03EE-42A6-B540-56CAE3AC19F6}resource=/crio_Mod3/Vsup Voltage;0;ReadMethodType=u16{C3745DB5-E97B-4E0B-BDD0-6E24EBE686D2}/name=Axis 3.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=20{C5FAB816-19EE-4821-B57F-41707B179D4F}/name=Axis 3.Mailbox (Host to FPGA)/datatype=5/direction=1/index=11{C8B9F7DD-C86F-4681-AF22-A375691DCB63}resource=/crio_Mod3/Phase B Neg;0;WriteMethodType=bool{C9538BF3-4301-4422-84DB-35B49B1312C2}resource=/crio_Mod4/Phase A Current;0;ReadMethodType=I16{CA2DB12E-EF97-4735-811B-F3D08E3AC19E}/name=Axis 2.DIO Status (Port A)/datatype=6/direction=0/index=11{CAF73AA4-6484-4F0F-BF41-F0C0EC242404}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=bool{CBE28825-E761-45A3-9161-325B01DEDB16}/name=Axis 1.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=2{D14BE3AE-4EFB-46E7-B0D7-0733C33D8736}/name=Axis 3.Control Register/datatype=6/direction=1/index=10{D29EB5F7-9DC9-4D9F-914A-F49B8E1608DD}resource=/crio_Mod1/DO14;0;ReadMethodType=bool;WriteMethodType=bool{D386CC9B-0765-4A90-8BD4-0E243C679022}resource=/crio_Mod2/Phase A Current;0;ReadMethodType=I16{D4B69A68-9DA8-4377-80B2-81EEBCBE57CA}resource=/crio_Mod1/DI15:8;0;ReadMethodType=u8{DA0FAA92-F7C0-45E7-958B-A2F6111D9869}/name=Axis 2.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=13{DC27D298-593E-4DE3-ADE3-28D6C6870856}/name=Axis 1.Position Feedback/datatype=2/direction=0/index=2{DC670331-7130-4FF8-ACD8-BB5DF85B84A1}/name=Axis 3.Status Register/datatype=6/direction=0/index=15{DFF36B8A-13B2-4D63-BF19-4BDAFB5DCD62}resource=/crio_Mod1/DI0;0;ReadMethodType=bool{E086F6D2-DAA0-4963-A0DB-2BCCDD71A8D5}resource=/crio_Mod1/DI9;0;ReadMethodType=bool{E25E9851-D552-488C-9B8D-D63A0813CB0D}resource=/crio_Mod4/Phase B Pos;0;WriteMethodType=bool{E302B98A-C798-4E3F-A53A-04207F12B35C}resource=/crio_Mod1/DI3;0;ReadMethodType=bool{E44C817D-F6DC-401A-85D4-F3B9ACD9A4D2}/name=Axis 2.Status Register/datatype=6/direction=0/index=8{E5EA3F98-15FB-40B1-B0A9-8249BCE88E60}resource=/crio_Mod1/DI4;0;ReadMethodType=bool{E65CB17F-1DF1-434E-A64E-FC1FC266D84C}resource=/crio_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=bool{E6B28907-AA6D-4D8C-BED7-04C972B9C9FF}/name=Axis 3.DIO Status (Port A)/datatype=6/direction=0/index=18{F0C3C458-8A6C-438A-BB19-5D62B28B9211}resource=/Reset RT App;0;WriteMethodType=bool{F2BCE5E9-4F44-40C2-BE47-22E1C137C971}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9503[crioConfig.End]{F6425AA6-51D3-4206-B29C-5B86EA0DE4A8}/name=Axis 3.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=12{F7C8008D-3B46-48F7-AB2C-528ADDD8D52A}resource=/crio_Mod1/DI6;0;ReadMethodType=bool{F7CCBC1F-27FF-4FAA-9C46-758896B69811}NumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=bool{F91B61AF-21BC-4BDF-A274-7CCB5F1F10E0}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{FC7B3B80-6ECB-4D16-A0EC-640A0D4FBC59}resource=/crio_Mod1/DI1;0;ReadMethodType=bool{FCE1384A-2AF9-40C5-96ED-46C5D20550DA}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=bool{FEDB15D4-5B84-4052-B9F6-F6AB023BC552}resource=/crio_Mod4/User LED;0;WriteMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">10 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool12.8 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool13.1072 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Axis 1.Control Register/name=Axis 1.Control Register/datatype=6/direction=1/index=0Axis 1.DIO Control (Port A)/name=Axis 1.DIO Control (Port A)/datatype=6/direction=1/index=3Axis 1.DIO Control (Port B)/name=Axis 1.DIO Control (Port B)/datatype=6/direction=1/index=4Axis 1.DIO Status (Port A)/name=Axis 1.DIO Status (Port A)/datatype=6/direction=0/index=4Axis 1.DIO Status (Port B)/name=Axis 1.DIO Status (Port B)/datatype=6/direction=0/index=5Axis 1.Mailbox (FPGA to Host)/name=Axis 1.Mailbox (FPGA to Host)/datatype=5/direction=0/index=0Axis 1.Mailbox (Host to FPGA)/name=Axis 1.Mailbox (Host to FPGA)/datatype=5/direction=1/index=1Axis 1.Position Feedback/name=Axis 1.Position Feedback/datatype=2/direction=0/index=2Axis 1.Position Setpoint/name=Axis 1.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=2Axis 1.Status Register/name=Axis 1.Status Register/datatype=6/direction=0/index=1Axis 1.Steps Generated/name=Axis 1.Steps Generated/datatype=2/direction=0/index=3Axis 1.Velocity Feedback/name=Axis 1.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=6Axis 2.Control Register/name=Axis 2.Control Register/datatype=6/direction=1/index=5Axis 2.DIO Control (Port A)/name=Axis 2.DIO Control (Port A)/datatype=6/direction=1/index=8Axis 2.DIO Control (Port B)/name=Axis 2.DIO Control (Port B)/datatype=6/direction=1/index=9Axis 2.DIO Status (Port A)/name=Axis 2.DIO Status (Port A)/datatype=6/direction=0/index=11Axis 2.DIO Status (Port B)/name=Axis 2.DIO Status (Port B)/datatype=6/direction=0/index=12Axis 2.Mailbox (FPGA to Host)/name=Axis 2.Mailbox (FPGA to Host)/datatype=5/direction=0/index=7Axis 2.Mailbox (Host to FPGA)/name=Axis 2.Mailbox (Host to FPGA)/datatype=5/direction=1/index=6Axis 2.Position Feedback/name=Axis 2.Position Feedback/datatype=2/direction=0/index=9Axis 2.Position Setpoint/name=Axis 2.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=7Axis 2.Status Register/name=Axis 2.Status Register/datatype=6/direction=0/index=8Axis 2.Steps Generated/name=Axis 2.Steps Generated/datatype=2/direction=0/index=10Axis 2.Velocity Feedback/name=Axis 2.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=13Axis 3.Control Register/name=Axis 3.Control Register/datatype=6/direction=1/index=10Axis 3.DIO Control (Port A)/name=Axis 3.DIO Control (Port A)/datatype=6/direction=1/index=13Axis 3.DIO Control (Port B)/name=Axis 3.DIO Control (Port B)/datatype=6/direction=1/index=14Axis 3.DIO Status (Port A)/name=Axis 3.DIO Status (Port A)/datatype=6/direction=0/index=18Axis 3.DIO Status (Port B)/name=Axis 3.DIO Status (Port B)/datatype=6/direction=0/index=19Axis 3.Mailbox (FPGA to Host)/name=Axis 3.Mailbox (FPGA to Host)/datatype=5/direction=0/index=14Axis 3.Mailbox (Host to FPGA)/name=Axis 3.Mailbox (Host to FPGA)/datatype=5/direction=1/index=11Axis 3.Position Feedback/name=Axis 3.Position Feedback/datatype=2/direction=0/index=16Axis 3.Position Setpoint/name=Axis 3.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=12Axis 3.Status Register/name=Axis 3.Status Register/datatype=6/direction=0/index=15Axis 3.Steps Generated/name=Axis 3.Steps Generated/datatype=2/direction=0/index=17Axis 3.Velocity Feedback/name=Axis 3.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=20Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO_Trig0ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig1ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig2ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig3ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig4NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=boolcRIO_Trig5NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=boolcRIO_Trig6NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=boolcRIO_Trig7NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Mod1/DI0resource=/crio_Mod1/DI0;0;ReadMethodType=boolMod1/DI10resource=/crio_Mod1/DI10;0;ReadMethodType=boolMod1/DI11resource=/crio_Mod1/DI11;0;ReadMethodType=boolMod1/DI12resource=/crio_Mod1/DI12;0;ReadMethodType=boolMod1/DI13resource=/crio_Mod1/DI13;0;ReadMethodType=boolMod1/DI14resource=/crio_Mod1/DI14;0;ReadMethodType=boolMod1/DI15:0resource=/crio_Mod1/DI15:0;0;ReadMethodType=u16Mod1/DI15:8resource=/crio_Mod1/DI15:8;0;ReadMethodType=u8Mod1/DI15resource=/crio_Mod1/DI15;0;ReadMethodType=boolMod1/DI1resource=/crio_Mod1/DI1;0;ReadMethodType=boolMod1/DI2resource=/crio_Mod1/DI2;0;ReadMethodType=boolMod1/DI3resource=/crio_Mod1/DI3;0;ReadMethodType=boolMod1/DI4resource=/crio_Mod1/DI4;0;ReadMethodType=boolMod1/DI5resource=/crio_Mod1/DI5;0;ReadMethodType=boolMod1/DI6resource=/crio_Mod1/DI6;0;ReadMethodType=boolMod1/DI7:0resource=/crio_Mod1/DI7:0;0;ReadMethodType=u8Mod1/DI7resource=/crio_Mod1/DI7;0;ReadMethodType=boolMod1/DI8resource=/crio_Mod1/DI8;0;ReadMethodType=boolMod1/DI9resource=/crio_Mod1/DI9;0;ReadMethodType=boolMod1/DO0resource=/crio_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO10resource=/crio_Mod1/DO10;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO11resource=/crio_Mod1/DO11;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO12resource=/crio_Mod1/DO12;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO13resource=/crio_Mod1/DO13;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO14resource=/crio_Mod1/DO14;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO15:0resource=/crio_Mod1/DO15:0;0;ReadMethodType=u16;WriteMethodType=u16Mod1/DO15:8resource=/crio_Mod1/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DO15resource=/crio_Mod1/DO15;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO1resource=/crio_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO2resource=/crio_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO3resource=/crio_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO4resource=/crio_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO5resource=/crio_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO6resource=/crio_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO7:0resource=/crio_Mod1/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DO7resource=/crio_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO8resource=/crio_Mod1/DO8;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO9resource=/crio_Mod1/DO9;0;ReadMethodType=bool;WriteMethodType=boolMod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9375,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod2/Phase A Currentresource=/crio_Mod2/Phase A Current;0;ReadMethodType=I16Mod2/Phase A Negresource=/crio_Mod2/Phase A Neg;0;WriteMethodType=boolMod2/Phase A Posresource=/crio_Mod2/Phase A Pos;0;WriteMethodType=boolMod2/Phase B Currentresource=/crio_Mod2/Phase B Current;0;ReadMethodType=I16Mod2/Phase B Negresource=/crio_Mod2/Phase B Neg;0;WriteMethodType=boolMod2/Phase B Posresource=/crio_Mod2/Phase B Pos;0;WriteMethodType=boolMod2/User LEDresource=/crio_Mod2/User LED;0;WriteMethodType=boolMod2/Vsup Voltageresource=/crio_Mod2/Vsup Voltage;0;ReadMethodType=u16Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9503[crioConfig.End]Mod3/Phase A Currentresource=/crio_Mod3/Phase A Current;0;ReadMethodType=I16Mod3/Phase A Negresource=/crio_Mod3/Phase A Neg;0;WriteMethodType=boolMod3/Phase A Posresource=/crio_Mod3/Phase A Pos;0;WriteMethodType=boolMod3/Phase B Currentresource=/crio_Mod3/Phase B Current;0;ReadMethodType=I16Mod3/Phase B Negresource=/crio_Mod3/Phase B Neg;0;WriteMethodType=boolMod3/Phase B Posresource=/crio_Mod3/Phase B Pos;0;WriteMethodType=boolMod3/User LEDresource=/crio_Mod3/User LED;0;WriteMethodType=boolMod3/Vsup Voltageresource=/crio_Mod3/Vsup Voltage;0;ReadMethodType=u16Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9503[crioConfig.End]Mod4/Phase A Currentresource=/crio_Mod4/Phase A Current;0;ReadMethodType=I16Mod4/Phase A Negresource=/crio_Mod4/Phase A Neg;0;WriteMethodType=boolMod4/Phase A Posresource=/crio_Mod4/Phase A Pos;0;WriteMethodType=boolMod4/Phase B Currentresource=/crio_Mod4/Phase B Current;0;ReadMethodType=I16Mod4/Phase B Negresource=/crio_Mod4/Phase B Neg;0;WriteMethodType=boolMod4/Phase B Posresource=/crio_Mod4/Phase B Pos;0;WriteMethodType=boolMod4/User LEDresource=/crio_Mod4/User LED;0;WriteMethodType=boolMod4/Vsup Voltageresource=/crio_Mod4/Vsup Voltage;0;ReadMethodType=u16Mod4[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9503[crioConfig.End]Offset from Time Reference ValidNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=boolOffset from Time ReferenceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32Reset RT Appresource=/Reset RT App;0;WriteMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolSystem Watchdog ExpiredNumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=boolTime SourceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctlTime Synchronization FaultNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=boolTimeNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64USER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool</Property>
					<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\Users\m.schmidt\Desktop\PROJ\3-achs-fräsmaschine\FPGA Bitfiles\3-Achs-Fräsmasch_FPGATarget_FPGA_z7PyS89HFDA.lvbitx</Property>
				</Item>
				<Item Name="DriveState.ctl" Type="VI" URL="../TypeDefs/DriveState.ctl">
					<Property Name="configString.guid" Type="Str">{0017E7D9-AF68-4A4D-BDCB-E0F80A595821}resource=/crio_Mod1/DI14;0;ReadMethodType=bool{01ADC950-F697-4E10-AC0F-3597AE707FCD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9375,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0337EAE1-9570-4F55-9DC5-6B925C4780EE}/name=Axis 2.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=7{03630C58-CD97-4F3E-8BE6-E29784853061}/name=Axis 1.DIO Control (Port B)/datatype=6/direction=1/index=4{047258CE-911D-42C2-A57D-DE27BB9E6FEC}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=bool{05A81A96-A932-488D-9473-888AF8C3AB12}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{09AB7A74-FAD4-439D-B637-9EF3ADAA2D7E}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64{0CF72225-8BCB-413F-AF38-51EA4654D1B6}resource=/crio_Mod3/User LED;0;WriteMethodType=bool{0D6D93D5-D434-4FF8-8977-5D8277810B89}resource=/crio_Mod2/Phase A Neg;0;WriteMethodType=bool{0DF8051B-C1F4-4EB5-898D-AF590BD87F25}/name=Axis 2.DIO Control (Port A)/datatype=6/direction=1/index=8{12F80F9D-1A19-493C-A636-E7CF8033A12E}resource=/crio_Mod1/DI15:0;0;ReadMethodType=u16{131E4BA5-61B4-4357-971A-4160B95447E5}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{13433233-B1E3-41EE-843C-6F079D8DA1CE}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=bool{15176B56-6AE9-44B1-B312-A218C64C0A3E}resource=/crio_Mod1/DI13;0;ReadMethodType=bool{1731DCD7-A5EC-4931-9123-1CE53C50FCB3}resource=/crio_Mod1/DI7;0;ReadMethodType=bool{18DCE8CD-C41D-4393-9EF9-0068DAF3E454}resource=/crio_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=bool{196A8036-32AD-4C7A-B6FD-2089C426A26F}resource=/crio_Mod1/DO9;0;ReadMethodType=bool;WriteMethodType=bool{1B64D359-A52B-4F6E-84B9-E2C60FBBF03B}resource=/Chassis Temperature;0;ReadMethodType=i16{1CBD5F71-337D-444F-A614-F4E3D2FC5266}resource=/crio_Mod3/Phase B Pos;0;WriteMethodType=bool{1E4D6A2C-5B1C-4B24-8F0F-D0DE4861E24C}resource=/crio_Mod1/DO15:0;0;ReadMethodType=u16;WriteMethodType=u16{1F296AF1-93A2-4B75-9EE0-63DE3E43B3CD}resource=/crio_Mod3/Phase B Current;0;ReadMethodType=I16{22F14F7B-7878-4735-8FFF-6E208B901DCB}resource=/crio_Mod2/User LED;0;WriteMethodType=bool{29E50DF6-D6C6-4E70-8683-467F7D2BA535}resource=/crio_Mod2/Phase B Pos;0;WriteMethodType=bool{2A7D546E-C337-47B4-86F2-EC01EB73A8D4}resource=/crio_Mod4/Phase A Neg;0;WriteMethodType=bool{2DC7761A-7739-4BFC-982B-7E45F47099A2}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32{2DE03463-12E0-48A1-B408-73CC91ED7D0A}/name=Axis 2.Position Feedback/datatype=2/direction=0/index=9{30AA07C1-223C-4AB8-A1A9-DB4EAE61F437}/name=Axis 3.DIO Control (Port A)/datatype=6/direction=1/index=13{311CCA04-9C8B-47AE-9291-2427298E19EF}resource=/crio_Mod2/Phase B Current;0;ReadMethodType=I16{33EE390F-78F9-400C-B704-58C0D7A04188}/name=Axis 2.Mailbox (Host to FPGA)/datatype=5/direction=1/index=6{34F7A81C-BA4C-400F-9078-70CD2104C790}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{36639CFF-6AB6-40BD-B425-DA84855425D7}NumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool{36A125B8-5220-4C6B-B2FC-0B3C2F2703F1}resource=/crio_Mod4/Vsup Voltage;0;ReadMethodType=u16{37B9D1F4-8948-4C7D-859E-840CB3C009E8}resource=/crio_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=bool{407E808C-C7FA-4964-9B71-E150D4214690}/name=Axis 1.Mailbox (Host to FPGA)/datatype=5/direction=1/index=1{4281C939-6849-4542-8375-DC854108F893}/name=Axis 3.DIO Status (Port B)/datatype=6/direction=0/index=19{4813C9AE-1626-40E2-A2D1-92C893C9653A}/name=Axis 2.Control Register/datatype=6/direction=1/index=5{4A0A5AD1-CC49-4854-9036-9503E724C780}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=bool{4A50DE0C-31A7-40D3-AFC5-1226FDA68401}/name=Axis 2.Mailbox (FPGA to Host)/datatype=5/direction=0/index=7{514991AE-3F66-447E-8071-01006692529D}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=bool{59CC0C88-22A1-4840-A476-5EC74AD59EEE}resource=/crio_Mod1/DI2;0;ReadMethodType=bool{59F18F21-D34A-440E-BF0C-DADCE1F6BEE6}/name=Axis 1.DIO Status (Port A)/datatype=6/direction=0/index=4{59F7A552-8DE9-40A7-BF40-124E0354177F}/name=Axis 3.Mailbox (FPGA to Host)/datatype=5/direction=0/index=14{5B08D5E4-F4FB-4A3B-85A6-B54AE2A3CB8F}resource=/crio_Mod4/Phase A Pos;0;WriteMethodType=bool{5C0C0298-27DB-474F-89DA-9CCEBB00E7B5}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=bool{5E79850A-008E-435E-8EC3-FC448F4B7752}/name=Axis 3.Steps Generated/datatype=2/direction=0/index=17{63CAEC4C-4375-4E8B-8D45-9BD560A34959}/name=Axis 1.DIO Control (Port A)/datatype=6/direction=1/index=3{66B47596-5DE0-43D1-B697-174EA01F359B}resource=/crio_Mod1/DI10;0;ReadMethodType=bool{697EF40C-5290-4619-AEE3-4E63DDA449D0}resource=/crio_Mod1/DI7:0;0;ReadMethodType=u8{6A747376-939C-40E7-BDB1-CBEA97D3007B}resource=/crio_Mod1/DO11;0;ReadMethodType=bool;WriteMethodType=bool{6CD02D79-DFB0-4E13-B8B0-4BF96F177BEF}resource=/crio_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=bool{6D91CD44-DB04-47C3-A62C-16857B5AA964}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=bool{6E3BA9CA-2C9B-4CB9-B653-CD2595A217C6}resource=/crio_Mod1/DO8;0;ReadMethodType=bool;WriteMethodType=bool{6F7A0866-03D4-4F01-BD82-CFD8BEFAFF79}resource=/crio_Mod4/Phase B Current;0;ReadMethodType=I16{6FBB5A63-EA42-449D-BBFE-CC2C51A9EAD7}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9503[crioConfig.End]{79BC5C5E-9EFF-4453-8DEC-800FCBFA2543}/name=Axis 3.DIO Control (Port B)/datatype=6/direction=1/index=14{7BBA44A1-0B1C-4D3B-8C34-77E5F8BFF19D}resource=/crio_Mod4/Phase B Neg;0;WriteMethodType=bool{7D03D325-9E75-4C28-B764-31C7691A4DDF}resource=/crio_Mod1/DO12;0;ReadMethodType=bool;WriteMethodType=bool{7F9F9F36-C1DE-4CF2-AABE-88FF5FFD8204}resource=/crio_Mod1/DO10;0;ReadMethodType=bool;WriteMethodType=bool{812C894E-0D98-4649-819A-AEA3ED0D8FB3}resource=/crio_Mod1/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{822F4B12-3504-4AB6-91CA-915DF3D744C7}resource=/crio_Mod2/Phase B Neg;0;WriteMethodType=bool{854317AC-436F-4D15-B272-1B28B36E5683}/name=Axis 1.Steps Generated/datatype=2/direction=0/index=3{8845D70A-3D8B-49E0-AEA5-EE3B8141E438}NumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool{88657F19-2BA2-492A-9398-45300D6B8A81}resource=/crio_Mod1/DI11;0;ReadMethodType=bool{89ADC391-1BC0-4EB7-9C7B-BEF6E0B4E2DE}resource=/crio_Mod1/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{92F4FB54-4EEB-4036-A6FB-964EFB5CE42D}resource=/crio_Mod1/DI15;0;ReadMethodType=bool{936546CE-563B-48D1-A9F9-78D46743E3C7}/name=Axis 1.Control Register/datatype=6/direction=1/index=0{9AD3274D-E096-44FC-808D-806587084876}/name=Axis 2.DIO Control (Port B)/datatype=6/direction=1/index=9{9C2165FA-8616-48F6-B956-1A396BED9378}resource=/crio_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=bool{9C71592D-D581-44ED-9946-F12827544A5C}resource=/crio_Mod3/Phase A Pos;0;WriteMethodType=bool{9DA89C10-2764-4628-9BFD-833BB10392D5}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9503[crioConfig.End]{9E0BFAF6-29A4-4BDA-8BB0-50EC671080B5}resource=/crio_Mod2/Phase A Pos;0;WriteMethodType=bool{9F5B802E-B199-494A-AFCA-E189C0C34B3B}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctl{9F67DCC6-8812-4A91-93DC-A6B351C8DC37}/name=Axis 2.DIO Status (Port B)/datatype=6/direction=0/index=12{A0AFFE3F-BCDE-458A-A2FB-6F87F5424577}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=bool{A0DCCCBC-384C-4A97-8439-C5B775CF3C25}resource=/crio_Mod3/Phase A Neg;0;WriteMethodType=bool{A0E4D4FD-3884-43CF-86A1-CCA8C2079A1B}/name=Axis 3.Position Feedback/datatype=2/direction=0/index=16{A6F7D6E6-D7C0-4797-90A0-440D9499B717}/name=Axis 1.Mailbox (FPGA to Host)/datatype=5/direction=0/index=0{AB2AC6E9-D72D-4F0F-8251-5F643F9B4E76}resource=/crio_Mod1/DI8;0;ReadMethodType=bool{AC2806CE-A965-48BF-8C21-64B5C17D41AC}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=bool{AE50F7E2-7756-426D-B141-7D02C6851358}resource=/crio_Mod1/DI5;0;ReadMethodType=bool{AE618064-30BD-4FD8-92F8-516F46545FBD}NumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool{B3C7F845-41F2-446C-8C5C-FE57C9CF43BD}/name=Axis 1.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=6{B3D43C34-FEFB-4D29-A9DC-E6790F105CD2}/name=Axis 1.DIO Status (Port B)/datatype=6/direction=0/index=5{B3F70B56-55F8-4007-B34E-DE323353B34B}/name=Axis 1.Status Register/datatype=6/direction=0/index=1{B40685F8-57B0-463E-954E-158889844CF4}resource=/crio_Mod2/Vsup Voltage;0;ReadMethodType=u16{B52805A6-988A-4800-B3FD-A91EDF13F323}resource=/crio_Mod1/DO13;0;ReadMethodType=bool;WriteMethodType=bool{B6256622-F944-4E75-BCCE-DAF23D644E65}/name=Axis 2.Steps Generated/datatype=2/direction=0/index=10{B9EA829D-B112-451B-A1E0-73B7EC92B72C}resource=/crio_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=bool{BA4668F3-3FB1-47DB-A134-D6649E9F5F72}resource=/crio_Mod3/Phase A Current;0;ReadMethodType=I16{BD3E74EB-6C84-406E-A6CC-0B3AF54DFFC8}resource=/crio_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=bool{BD8B5BF3-B28D-4C14-BC4E-A90CFCE7EAF3}resource=/crio_Mod1/DI12;0;ReadMethodType=bool{BDCB389F-C80C-458B-B7C6-8B848095CF6C}resource=/crio_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=bool{BF74F566-726D-4FC6-9AA1-F751E6293481}resource=/crio_Mod1/DO15;0;ReadMethodType=bool;WriteMethodType=bool{BFC9F9C2-196A-4953-BC5D-FAC0CECDEA1E}resource=/Scan Clock;0;ReadMethodType=bool{C343CFF2-03EE-42A6-B540-56CAE3AC19F6}resource=/crio_Mod3/Vsup Voltage;0;ReadMethodType=u16{C3745DB5-E97B-4E0B-BDD0-6E24EBE686D2}/name=Axis 3.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=20{C5FAB816-19EE-4821-B57F-41707B179D4F}/name=Axis 3.Mailbox (Host to FPGA)/datatype=5/direction=1/index=11{C8B9F7DD-C86F-4681-AF22-A375691DCB63}resource=/crio_Mod3/Phase B Neg;0;WriteMethodType=bool{C9538BF3-4301-4422-84DB-35B49B1312C2}resource=/crio_Mod4/Phase A Current;0;ReadMethodType=I16{CA2DB12E-EF97-4735-811B-F3D08E3AC19E}/name=Axis 2.DIO Status (Port A)/datatype=6/direction=0/index=11{CAF73AA4-6484-4F0F-BF41-F0C0EC242404}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=bool{CBE28825-E761-45A3-9161-325B01DEDB16}/name=Axis 1.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=2{D14BE3AE-4EFB-46E7-B0D7-0733C33D8736}/name=Axis 3.Control Register/datatype=6/direction=1/index=10{D29EB5F7-9DC9-4D9F-914A-F49B8E1608DD}resource=/crio_Mod1/DO14;0;ReadMethodType=bool;WriteMethodType=bool{D386CC9B-0765-4A90-8BD4-0E243C679022}resource=/crio_Mod2/Phase A Current;0;ReadMethodType=I16{D4B69A68-9DA8-4377-80B2-81EEBCBE57CA}resource=/crio_Mod1/DI15:8;0;ReadMethodType=u8{DA0FAA92-F7C0-45E7-958B-A2F6111D9869}/name=Axis 2.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=13{DC27D298-593E-4DE3-ADE3-28D6C6870856}/name=Axis 1.Position Feedback/datatype=2/direction=0/index=2{DC670331-7130-4FF8-ACD8-BB5DF85B84A1}/name=Axis 3.Status Register/datatype=6/direction=0/index=15{DFF36B8A-13B2-4D63-BF19-4BDAFB5DCD62}resource=/crio_Mod1/DI0;0;ReadMethodType=bool{E086F6D2-DAA0-4963-A0DB-2BCCDD71A8D5}resource=/crio_Mod1/DI9;0;ReadMethodType=bool{E25E9851-D552-488C-9B8D-D63A0813CB0D}resource=/crio_Mod4/Phase B Pos;0;WriteMethodType=bool{E302B98A-C798-4E3F-A53A-04207F12B35C}resource=/crio_Mod1/DI3;0;ReadMethodType=bool{E44C817D-F6DC-401A-85D4-F3B9ACD9A4D2}/name=Axis 2.Status Register/datatype=6/direction=0/index=8{E5EA3F98-15FB-40B1-B0A9-8249BCE88E60}resource=/crio_Mod1/DI4;0;ReadMethodType=bool{E65CB17F-1DF1-434E-A64E-FC1FC266D84C}resource=/crio_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=bool{E6B28907-AA6D-4D8C-BED7-04C972B9C9FF}/name=Axis 3.DIO Status (Port A)/datatype=6/direction=0/index=18{F0C3C458-8A6C-438A-BB19-5D62B28B9211}resource=/Reset RT App;0;WriteMethodType=bool{F2BCE5E9-4F44-40C2-BE47-22E1C137C971}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9503[crioConfig.End]{F6425AA6-51D3-4206-B29C-5B86EA0DE4A8}/name=Axis 3.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=12{F7C8008D-3B46-48F7-AB2C-528ADDD8D52A}resource=/crio_Mod1/DI6;0;ReadMethodType=bool{F7CCBC1F-27FF-4FAA-9C46-758896B69811}NumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=bool{F91B61AF-21BC-4BDF-A274-7CCB5F1F10E0}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{FC7B3B80-6ECB-4D16-A0EC-640A0D4FBC59}resource=/crio_Mod1/DI1;0;ReadMethodType=bool{FCE1384A-2AF9-40C5-96ED-46C5D20550DA}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=bool{FEDB15D4-5B84-4052-B9F6-F6AB023BC552}resource=/crio_Mod4/User LED;0;WriteMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">10 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool12.8 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool13.1072 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Axis 1.Control Register/name=Axis 1.Control Register/datatype=6/direction=1/index=0Axis 1.DIO Control (Port A)/name=Axis 1.DIO Control (Port A)/datatype=6/direction=1/index=3Axis 1.DIO Control (Port B)/name=Axis 1.DIO Control (Port B)/datatype=6/direction=1/index=4Axis 1.DIO Status (Port A)/name=Axis 1.DIO Status (Port A)/datatype=6/direction=0/index=4Axis 1.DIO Status (Port B)/name=Axis 1.DIO Status (Port B)/datatype=6/direction=0/index=5Axis 1.Mailbox (FPGA to Host)/name=Axis 1.Mailbox (FPGA to Host)/datatype=5/direction=0/index=0Axis 1.Mailbox (Host to FPGA)/name=Axis 1.Mailbox (Host to FPGA)/datatype=5/direction=1/index=1Axis 1.Position Feedback/name=Axis 1.Position Feedback/datatype=2/direction=0/index=2Axis 1.Position Setpoint/name=Axis 1.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=2Axis 1.Status Register/name=Axis 1.Status Register/datatype=6/direction=0/index=1Axis 1.Steps Generated/name=Axis 1.Steps Generated/datatype=2/direction=0/index=3Axis 1.Velocity Feedback/name=Axis 1.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=6Axis 2.Control Register/name=Axis 2.Control Register/datatype=6/direction=1/index=5Axis 2.DIO Control (Port A)/name=Axis 2.DIO Control (Port A)/datatype=6/direction=1/index=8Axis 2.DIO Control (Port B)/name=Axis 2.DIO Control (Port B)/datatype=6/direction=1/index=9Axis 2.DIO Status (Port A)/name=Axis 2.DIO Status (Port A)/datatype=6/direction=0/index=11Axis 2.DIO Status (Port B)/name=Axis 2.DIO Status (Port B)/datatype=6/direction=0/index=12Axis 2.Mailbox (FPGA to Host)/name=Axis 2.Mailbox (FPGA to Host)/datatype=5/direction=0/index=7Axis 2.Mailbox (Host to FPGA)/name=Axis 2.Mailbox (Host to FPGA)/datatype=5/direction=1/index=6Axis 2.Position Feedback/name=Axis 2.Position Feedback/datatype=2/direction=0/index=9Axis 2.Position Setpoint/name=Axis 2.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=7Axis 2.Status Register/name=Axis 2.Status Register/datatype=6/direction=0/index=8Axis 2.Steps Generated/name=Axis 2.Steps Generated/datatype=2/direction=0/index=10Axis 2.Velocity Feedback/name=Axis 2.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=13Axis 3.Control Register/name=Axis 3.Control Register/datatype=6/direction=1/index=10Axis 3.DIO Control (Port A)/name=Axis 3.DIO Control (Port A)/datatype=6/direction=1/index=13Axis 3.DIO Control (Port B)/name=Axis 3.DIO Control (Port B)/datatype=6/direction=1/index=14Axis 3.DIO Status (Port A)/name=Axis 3.DIO Status (Port A)/datatype=6/direction=0/index=18Axis 3.DIO Status (Port B)/name=Axis 3.DIO Status (Port B)/datatype=6/direction=0/index=19Axis 3.Mailbox (FPGA to Host)/name=Axis 3.Mailbox (FPGA to Host)/datatype=5/direction=0/index=14Axis 3.Mailbox (Host to FPGA)/name=Axis 3.Mailbox (Host to FPGA)/datatype=5/direction=1/index=11Axis 3.Position Feedback/name=Axis 3.Position Feedback/datatype=2/direction=0/index=16Axis 3.Position Setpoint/name=Axis 3.Position Setpointsigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=1/index=12Axis 3.Status Register/name=Axis 3.Status Register/datatype=6/direction=0/index=15Axis 3.Steps Generated/name=Axis 3.Steps Generated/datatype=2/direction=0/index=17Axis 3.Velocity Feedback/name=Axis 3.Velocity Feedbacksigned=1/intWordLen=32/fractWordLen=32/delta=1x2^-32/rangeMax=7FFFFFFFFFFFFFFFx2^-32/rangeMin=8000000000000000x2^-32/overflow=0/direction=0/index=20Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO_Trig0ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig1ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig2ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig3ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig4NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=boolcRIO_Trig5NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=boolcRIO_Trig6NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=boolcRIO_Trig7NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Mod1/DI0resource=/crio_Mod1/DI0;0;ReadMethodType=boolMod1/DI10resource=/crio_Mod1/DI10;0;ReadMethodType=boolMod1/DI11resource=/crio_Mod1/DI11;0;ReadMethodType=boolMod1/DI12resource=/crio_Mod1/DI12;0;ReadMethodType=boolMod1/DI13resource=/crio_Mod1/DI13;0;ReadMethodType=boolMod1/DI14resource=/crio_Mod1/DI14;0;ReadMethodType=boolMod1/DI15:0resource=/crio_Mod1/DI15:0;0;ReadMethodType=u16Mod1/DI15:8resource=/crio_Mod1/DI15:8;0;ReadMethodType=u8Mod1/DI15resource=/crio_Mod1/DI15;0;ReadMethodType=boolMod1/DI1resource=/crio_Mod1/DI1;0;ReadMethodType=boolMod1/DI2resource=/crio_Mod1/DI2;0;ReadMethodType=boolMod1/DI3resource=/crio_Mod1/DI3;0;ReadMethodType=boolMod1/DI4resource=/crio_Mod1/DI4;0;ReadMethodType=boolMod1/DI5resource=/crio_Mod1/DI5;0;ReadMethodType=boolMod1/DI6resource=/crio_Mod1/DI6;0;ReadMethodType=boolMod1/DI7:0resource=/crio_Mod1/DI7:0;0;ReadMethodType=u8Mod1/DI7resource=/crio_Mod1/DI7;0;ReadMethodType=boolMod1/DI8resource=/crio_Mod1/DI8;0;ReadMethodType=boolMod1/DI9resource=/crio_Mod1/DI9;0;ReadMethodType=boolMod1/DO0resource=/crio_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO10resource=/crio_Mod1/DO10;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO11resource=/crio_Mod1/DO11;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO12resource=/crio_Mod1/DO12;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO13resource=/crio_Mod1/DO13;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO14resource=/crio_Mod1/DO14;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO15:0resource=/crio_Mod1/DO15:0;0;ReadMethodType=u16;WriteMethodType=u16Mod1/DO15:8resource=/crio_Mod1/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DO15resource=/crio_Mod1/DO15;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO1resource=/crio_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO2resource=/crio_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO3resource=/crio_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO4resource=/crio_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO5resource=/crio_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO6resource=/crio_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO7:0resource=/crio_Mod1/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DO7resource=/crio_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO8resource=/crio_Mod1/DO8;0;ReadMethodType=bool;WriteMethodType=boolMod1/DO9resource=/crio_Mod1/DO9;0;ReadMethodType=bool;WriteMethodType=boolMod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9375,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod2/Phase A Currentresource=/crio_Mod2/Phase A Current;0;ReadMethodType=I16Mod2/Phase A Negresource=/crio_Mod2/Phase A Neg;0;WriteMethodType=boolMod2/Phase A Posresource=/crio_Mod2/Phase A Pos;0;WriteMethodType=boolMod2/Phase B Currentresource=/crio_Mod2/Phase B Current;0;ReadMethodType=I16Mod2/Phase B Negresource=/crio_Mod2/Phase B Neg;0;WriteMethodType=boolMod2/Phase B Posresource=/crio_Mod2/Phase B Pos;0;WriteMethodType=boolMod2/User LEDresource=/crio_Mod2/User LED;0;WriteMethodType=boolMod2/Vsup Voltageresource=/crio_Mod2/Vsup Voltage;0;ReadMethodType=u16Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9503[crioConfig.End]Mod3/Phase A Currentresource=/crio_Mod3/Phase A Current;0;ReadMethodType=I16Mod3/Phase A Negresource=/crio_Mod3/Phase A Neg;0;WriteMethodType=boolMod3/Phase A Posresource=/crio_Mod3/Phase A Pos;0;WriteMethodType=boolMod3/Phase B Currentresource=/crio_Mod3/Phase B Current;0;ReadMethodType=I16Mod3/Phase B Negresource=/crio_Mod3/Phase B Neg;0;WriteMethodType=boolMod3/Phase B Posresource=/crio_Mod3/Phase B Pos;0;WriteMethodType=boolMod3/User LEDresource=/crio_Mod3/User LED;0;WriteMethodType=boolMod3/Vsup Voltageresource=/crio_Mod3/Vsup Voltage;0;ReadMethodType=u16Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9503[crioConfig.End]Mod4/Phase A Currentresource=/crio_Mod4/Phase A Current;0;ReadMethodType=I16Mod4/Phase A Negresource=/crio_Mod4/Phase A Neg;0;WriteMethodType=boolMod4/Phase A Posresource=/crio_Mod4/Phase A Pos;0;WriteMethodType=boolMod4/Phase B Currentresource=/crio_Mod4/Phase B Current;0;ReadMethodType=I16Mod4/Phase B Negresource=/crio_Mod4/Phase B Neg;0;WriteMethodType=boolMod4/Phase B Posresource=/crio_Mod4/Phase B Pos;0;WriteMethodType=boolMod4/User LEDresource=/crio_Mod4/User LED;0;WriteMethodType=boolMod4/Vsup Voltageresource=/crio_Mod4/Vsup Voltage;0;ReadMethodType=u16Mod4[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9503[crioConfig.End]Offset from Time Reference ValidNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=boolOffset from Time ReferenceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32Reset RT Appresource=/Reset RT App;0;WriteMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolSystem Watchdog ExpiredNumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=boolTime SourceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctlTime Synchronization FaultNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=boolTimeNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64USER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool</Property>
				</Item>
				<Item Name="Dependencies" Type="Dependencies">
					<Item Name="vi.lib" Type="Folder">
						<Item Name="NI_SoftMotion_MotorControlIP.lvlib" Type="Library" URL="/&lt;vilib&gt;/Motion/MotorControl/NI_SoftMotion_MotorControlIP.lvlib"/>
						<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
					</Item>
					<Item Name="9503 Scale current.vi" Type="VI" URL="../support/9503 Scale current.vi"/>
					<Item Name="PI Current nonreetrant.vi" Type="VI" URL="../support/PI Current nonreetrant.vi"/>
				</Item>
				<Item Name="Build Specifications" Type="Build">
					<Item Name="FPGA" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">FPGA</Property>
						<Property Name="Comp.BitfileName" Type="Str">3-Achs-Fräsmasch_FPGATarget_FPGA_z7PyS89HFDA.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">0</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/Users/m.schmidt/Desktop/PROJ/3-achs-fräsmaschine/FPGA Bitfiles/3-Achs-Fräsmasch_FPGATarget_FPGA_z7PyS89HFDA.lvbitx</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/3-Achs-Fräsmasch_FPGATarget_FPGA_z7PyS89HFDA.lvbitx</Property>
						<Property Name="ProjectPath" Type="Path">/C/Users/m.schmidt/Desktop/PROJ/3-achs-fräsmaschine/3-Achs-Fräsmaschine.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">true</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
						<Property Name="TargetName" Type="Str">FPGA Target</Property>
						<Property Name="TopLevelVI" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/FPGA Target/FPGA.vi</Property>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="DebugMode.vi" Type="VI" URL="../SubVIs/DebugMode.vi"/>
		<Item Name="Init.vi" Type="VI" URL="../SubVIs/Init.vi"/>
		<Item Name="Motor 1 Drive.vi" Type="VI" URL="../SubVIs/Motor 1 Drive.vi"/>
		<Item Name="Motor 2 Drive.vi" Type="VI" URL="../SubVIs/Motor 2 Drive.vi"/>
		<Item Name="Motor 3 Drive.vi" Type="VI" URL="../SubVIs/Motor 3 Drive.vi"/>
		<Item Name="MotorDriver.vi" Type="VI" URL="../SubVIs/MotorDriver.vi"/>
		<Item Name="MotorStats.ctl" Type="VI" URL="../TypeDefs/MotorStats.ctl"/>
		<Item Name="ProcessData.ctl" Type="VI" URL="../TypeDefs/ProcessData.ctl"/>
		<Item Name="Realtime-State.ctl" Type="VI" URL="../TypeDefs/Realtime-State.ctl"/>
		<Item Name="RT-MotorData.ctl" Type="VI" URL="../TypeDefs/RT-MotorData.ctl"/>
		<Item Name="RT.vi" Type="VI" URL="../RT/RT.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_SoftMotion_MotorControlIP.lvlib" Type="Library" URL="/&lt;vilib&gt;/Motion/MotorControl/NI_SoftMotion_MotorControlIP.lvlib"/>
			</Item>
			<Item Name="Calculate Current Loop Gains Set 1.vi" Type="VI" URL="../support/Calculate Current Loop Gains Set 1.vi"/>
			<Item Name="Calculate Current Scheduler Coefficients.vi" Type="VI" URL="../support/Calculate Current Scheduler Coefficients.vi"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Zero with Tolerance.vi" Type="VI" URL="../support/Zero with Tolerance.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Standalone-RT" Type="{69A947D5-514E-4E75-818E-69657C0547D8}">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{0EFF137F-C535-493E-BC1A-946C41B04634}</Property>
				<Property Name="App_INI_GUID" Type="Str">{27E22092-7000-473A-AC43-6363E7B09908}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{3374EA36-6F17-4178-B977-FB10BDAA5F32}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Standalone-RT</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/NI_AB_TARGETNAME/Standalone-RT</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{54AC1FCA-BC74-4506-A400-01342A2DF706}</Property>
				<Property Name="Bld_targetDestDir" Type="Path">/home/lvuser/natinst/bin</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">startup.rtexe</Property>
				<Property Name="Destination[0].path" Type="Path">/home/lvuser/natinst/bin/startup.rtexe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/home/lvuser/natinst/bin/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{0045A3B4-A7D8-4C30-AC80-DFFA6092EA61}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/RT.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/Realtime-State.ctl</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/RT-MotorData.ctl</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/DebugMode.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/Init.vi</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/Motor 1 Drive.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/Motor 2 Drive.vi</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/Motor 3 Drive.vi</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/MotorDriver.vi</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/MotorStats.ctl</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/NI-cRIO-9056-01DEC88F/ProcessData.ctl</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">12</Property>
				<Property Name="TgtF_companyName" Type="Str">System Entwicklung Nordhausen GmbH</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Standalone-RT</Property>
				<Property Name="TgtF_internalName" Type="Str">Standalone-RT</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019 System Entwicklung Nordhausen GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">Standalone-RT</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{CB3EEB39-542C-4B13-AB71-59AFD09DECD1}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">startup.rtexe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
